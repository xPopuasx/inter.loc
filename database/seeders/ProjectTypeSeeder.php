<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Многоквартирный дом',
                'slug' => 'apartment_house'
            ],
            [
                'title' => 'Котедж',
                'slug' => 'cottage'
            ],
            [
                'title' => 'Торговый центр',
                'slug' => 'shopping_center'
            ],
        ];

        foreach ($data as $type){
            \App\Models\ProjectType::create($type);
        }
    }
}
