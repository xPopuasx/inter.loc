<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->comment('ФИО пользователя');
            $table->mediumText('description')->nullable()->comment('Текст пользователя');
            $table->string('email')->comment('Почта пользователя');
            $table->string('phone')->comment('Телефон пользователя');
            $table->enum('status',['create', 'read', 'reject', 'complete'])->comment('Статус заявки');
            $table->enum('type',['phone', 'message'])->comment('Статус заявки');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
