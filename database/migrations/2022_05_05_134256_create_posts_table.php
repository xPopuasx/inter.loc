<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->text('title')->comment('Название поста');
            $table->text('slug')->comment('Слаг поста');
            $table->mediumText('description')->comment('Описание поста');
            $table->text('score')->nullable()->comment('Источник поста');
            $table->bigInteger('tag_id')->unsigned()->comment('Внешний ключ на тэг');
            $table->foreign('tag_id')->on('tags')->references('id');
            $table->bigInteger('user_id')->unsigned()->comment('Внешний ключ на пользователя');
            $table->foreign('user_id')->on('users')->references('id');
            $table->bigInteger('status_id')->unsigned()->comment('Внешний ключ на статус');
            $table->foreign('status_id')->on('statuses')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
