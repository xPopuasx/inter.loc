<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floors', function (Blueprint $table) {
            $table->id();
            $table->integer('row_id')->comment('id координаты на картинке проекта');
            $table->text('coords')->comment('Координата на изображении проекта');
            $table->integer('number')->comment('Номер этажа');
            $table->integer('apartments_in_the_floor')->comment('Количество квартир в подьезде');
            $table->string('image_floor')->comment('ссылка на план подъезда');
            $table->bigInteger('house_id')->unsigned()->comment('Внешний ключ на пользователя');
            $table->foreign('house_id')->on('houses')->references('id')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned()->nullable()->comment('Внешний ключ на пользователя');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floors');
    }
}
