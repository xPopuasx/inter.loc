<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Название проекта');
            $table->text('description')->comment('Описание проекта');
            $table->string('address')->comment('Адрес проекта');
            $table->string('lat')->comment('Широта на карте');
            $table->string('lng')->comment('Долгота на карте');
            $table->integer('quarter')->comment('Квартал сдачи');
            $table->string('type_id')->comment('Тип проекта');
            $table->year('delivery_year')->comment('Год сдачи');
            $table->integer('project_readiness')->nullable()->comment('Готовность проекта в процентах');
            $table->string('status_create_info')->nullable()->comment('Готовность проекта для отображения на сайте');
            $table->bigInteger('user_id')->unsigned()->nullable()->comment('id создавшего пользователя');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
