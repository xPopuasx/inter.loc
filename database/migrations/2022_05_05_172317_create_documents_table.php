<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('file_name')->comment('Название документа');
            $table->bigInteger('tag_id')->unsigned()->comment('Внешний ключ на тэг');
            $table->foreign('tag_id')->on('tags')->references('id');
            $table->bigInteger('user_id')->unsigned()->comment('Внешний ключ на пользователя');
            $table->foreign('user_id')->on('users')->references('id');
            $table->bigInteger('status_id')->unsigned()->comment('Внешний ключ на статус');
            $table->foreign('status_id')->on('statuses')->references('id');
            $table->integer('attachment_id')->unsigned()->comment('Внешний ключ на документ');
            $table->foreign('attachment_id')->on('attachments')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
