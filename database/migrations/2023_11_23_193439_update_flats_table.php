<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flats', function (Blueprint $table) {
            $table->integer('number')->comment('Номер квартиры')->nullable();
            $table->integer('price')->comment('Цена')->nullable();
            $table->integer('discount')->comment('Скидка')->nullable();
            $table->integer('status')->comment('Статус')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flats', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->dropColumn('price');
            $table->dropColumn('discount');
            $table->dropColumn('status');
        });
    }
}
