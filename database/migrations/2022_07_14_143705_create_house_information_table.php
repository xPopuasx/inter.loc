<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_information', function (Blueprint $table) {
            $table->id();
            $table->boolean('has_underground_parking')->nullable()->comment('Имеется ли подземная парковка');
            $table->boolean('has_parking')->nullable()->comment('Имеется ли парковка');
            $table->boolean('has_playground')->nullable()->comment('Имеется ли деская площадка');
            $table->boolean('has_fenced_area')->nullable()->comment('Имеется ли огороженная территория');
            $table->boolean('has_arrangement_of_the_territory')->nullable()->comment('Имеется ли благоустройстов территории');
            $table->boolean('has_concierge_system')->nullable()->comment('Имеется ли консьерж система');
            $table->integer('total_square_meters')->nullable()->comment('Количество квадратных метров');
            $table->integer('number_of_floors')->nullable()->comment('Количество этажей');
            $table->integer('parking_places')->nullable()->comment('Количество парковочных мест');
            $table->integer('number_of_apartments')->nullable()->comment('Количество квартир');
            $table->integer('number_of_entrances')->nullable()->comment('Количество подъездов');
            $table->string('project_image')->nullable()->comment('Ссылка на главную картинку');
            $table->bigInteger('house_id')->unsigned();
            $table->foreign('house_id')->references('id')->on('houses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_information');
    }
}
