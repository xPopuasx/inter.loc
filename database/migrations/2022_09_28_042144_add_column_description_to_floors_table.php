<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDescriptionToFloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('floors', function (Blueprint $table) {
            $table->text('description')->nullable()->after('image_floor');
        });

        Schema::table('flats', function (Blueprint $table) {
            $table->text('description')->nullable()->after('image_flat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('floors', function (Blueprint $table) {
            $table->dropColumn('description');
        });

        Schema::table('flats', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
