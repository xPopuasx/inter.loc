<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flats', function (Blueprint $table) {
            $table->id();
            $table->integer('row_id')->comment('id координаты на картинке проекта');
            $table->text('coords')->comment('Координата на изображении проекта');
            $table->float('square_meters')->comment('Квадратные метры');
            $table->float('square_meters_kitchen')->comment('Квадратные метры кухни');
            $table->float('square_meters_bathroom')->comment('Квадратные метры кухни');
            $table->integer('number_of_rooms')->comment('Количество комнат');
            $table->boolean('has_balcony')->nullable()->comment('Имеется балкон');
            $table->boolean('has_loggia')->nullable()->comment('Имеется лоджия');
            $table->boolean('has_free_layout')->nullable()->comment('Свободная планировка');
            $table->boolean('has_separate_bathroom')->nullable()->comment('Раздельный санузел');
            $table->string('image_flat')->comment('Картинка квартиры');
            $table->bigInteger('house_id')->unsigned()->comment('Внешний ключ на пользователя');
            $table->foreign('house_id')->on('houses')->references('id')->onDelete('cascade');
            $table->bigInteger('floor_id')->unsigned()->comment('Внешний ключ на пользователя');
            $table->foreign('floor_id')->on('floors')->references('id')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned()->nullable()->comment('Внешний ключ на пользователя');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flats');
    }
}
