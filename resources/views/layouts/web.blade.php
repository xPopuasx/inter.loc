<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="/images/icon.ico">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <link rel="stylesheet" href="/vendor/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/vendor/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="/vendor/css/pe-icon-7-stroke.css" />
    <link href="{{mix('css/project.css')}}" rel="stylesheet" type="text/css" >
</head>
<body data-bs-target=".navbar" data-bs-offset="110">

<div id="app"></div>
</body>
<script src="/js/vendor/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="/js/vendor/feather.js" type="text/javascript"></script>
<script src="/js/vendor/app.js" type="text/javascript"></script>
<script src="/js/app.js" type="text/javascript"></script>

</html>
