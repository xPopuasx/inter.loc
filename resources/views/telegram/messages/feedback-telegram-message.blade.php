Заявка со старницы {{$url}}

@if(isset($type))
<b>Тип сообщения:</b> {{$type}}
@endif
<b>ФИО:</b> {{$full_name}}
@if(isset($phone))
<b>Телефон:</b> {{$phone}}
@endif
@if(isset($description))
<b>Приложенное сообщение:</b> {{$description}}
@endif

