<div class="bg-white rounded-top shadow-sm mb-3">
    <div class="row g-0">
        <div class="col col-lg-12 mt-6 p-4">
            <div class="mt-2 text-dark fw-light">
                <ul class="nav command-bar justify-content-sm-end justify-content-start d-flex align-items-center">
                    <li>
                        <div class="form-group mb-0">
                            <a data-turbo="true" class="btn btn-link" id="finishApartment">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em"
                                     viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor"
                                     componentname="orchid-icon">
                                    <path
                                        d="M16 0c-8.836 0-16 7.163-16 16s7.163 16 16 16c8.837 0 16-7.163 16-16s-7.163-16-16-16zM16 30.032c-7.72 0-14-6.312-14-14.032s6.28-14 14-14 14 6.28 14 14-6.28 14.032-14 14.032zM23 15h-6v-6c0-0.552-0.448-1-1-1s-1 0.448-1 1v6h-6c-0.552 0-1 0.448-1 1s0.448 1 1 1h6v6c0 0.552 0.448 1 1 1s1-0.448 1-1v-6h6c0.552 0 1-0.448 1-1s-0.448-1-1-1z"></path>
                                </svg>
                                <i class="fa-header-1"></i>
                                Добавить фигуру (Enter)
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="form-group mb-0">
                            <a data-turbo="true" class="btn btn-link" id="clearLastPoint">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em"
                                     viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor"
                                     componentname="orchid-icon">
                                    <path
                                        d="M1.060 29.448c0.010 0 0.022 0 0.034-0.001 0.506-0.017 0.825-0.409 0.868-0.913 0.034-0.371 1.030-9.347 15.039-9.337l0.032 5.739c0 0.387 0.223 0.739 0.573 0.904 0.346 0.166 0.764 0.115 1.061-0.132l12.968-10.743c0.233-0.191 0.366-0.475 0.365-0.774s-0.136-0.584-0.368-0.774l-12.967-10.643c-0.299-0.244-0.712-0.291-1.061-0.128-0.349 0.166-0.572 0.518-0.572 0.903l-0.032 5.614c-5.811 0.184-10.312 2.053-13.229 5.467-4.748 5.556-3.688 13.63-3.639 13.966 0.074 0.49 0.433 0.85 0.926 0.85zM18.033 17.182h-0.002c-10.007 0.006-13.831 3.385-16.015 6.37 0.32-2.39 1.252-5.273 3.281-7.626 2.698-3.128 7.045-4.777 12.736-4.777 0.552 0 1-0.447 1-1v-4.493l10.389 8.542-10.389 8.622v-4.637c0-0.265-0.105-0.52-0.294-0.708-0.187-0.187-0.441-0.293-0.706-0.293z"></path>
                                </svg>
                                Удалить последнюю точку (Ctrl + Z)
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="form-group mb-0">
                            <a data-turbo="true" class="btn btn-link" id="clearLastFigure">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em"
                                     viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor"
                                     componentname="orchid-icon">
                                    <path
                                        d="M1.060 29.448c0.010 0 0.022 0 0.034-0.001 0.506-0.017 0.825-0.409 0.868-0.913 0.034-0.371 1.030-9.347 15.039-9.337l0.032 5.739c0 0.387 0.223 0.739 0.573 0.904 0.346 0.166 0.764 0.115 1.061-0.132l12.968-10.743c0.233-0.191 0.366-0.475 0.365-0.774s-0.136-0.584-0.368-0.774l-12.967-10.643c-0.299-0.244-0.712-0.291-1.061-0.128-0.349 0.166-0.572 0.518-0.572 0.903l-0.032 5.614c-5.811 0.184-10.312 2.053-13.229 5.467-4.748 5.556-3.688 13.63-3.639 13.966 0.074 0.49 0.433 0.85 0.926 0.85zM18.033 17.182h-0.002c-10.007 0.006-13.831 3.385-16.015 6.37 0.32-2.39 1.252-5.273 3.281-7.626 2.698-3.128 7.045-4.777 12.736-4.777 0.552 0 1-0.447 1-1v-4.493l10.389 8.542-10.389 8.622v-4.637c0-0.265-0.105-0.52-0.294-0.708-0.187-0.187-0.441-0.293-0.706-0.293z"></path>
                                </svg>
                                Удалить последнюю фигуру (Ctrl + Shift + Z)
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="form-group mb-0">
                            <a data-turbo="true" class="btn btn-remove" id="clearAll">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em"
                                     viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor"
                                     componentname="orchid-icon">
                                    <path
                                        d="M28.025 4.97l-7.040 0v-2.727c0-1.266-1.032-2.265-2.298-2.265h-5.375c-1.267 0-2.297 0.999-2.297 2.265v2.727h-7.040c-0.552 0-1 0.448-1 1s0.448 1 1 1h1.375l2.32 23.122c0.097 1.082 1.019 1.931 2.098 1.931h12.462c1.079 0 2-0.849 2.096-1.921l2.322-23.133h1.375c0.552 0 1-0.448 1-1s-0.448-1-1-1zM13.015 2.243c0-0.163 0.133-0.297 0.297-0.297h5.374c0.164 0 0.298 0.133 0.298 0.297v2.727h-5.97zM22.337 29.913c-0.005 0.055-0.070 0.11-0.105 0.11h-12.463c-0.035 0-0.101-0.055-0.107-0.12l-2.301-22.933h17.279z"></path>
                                </svg>
                                Отчистить всё
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row g-0" style="position: relative;">
        <div class="col col-lg-12 mt-6 p-4" style="position: relative;">
            <div class="container-image">
                <img src="{{$image}}" style="width: 100%; display: block;">
                <canvas id="overlay"></canvas>
            </div>
        </div>
    </div>
    <table class="matrix table table-bordered " data-controller="matrix"
           data-remove-url="flats-image-markup/removeLine" data-save-url="flats-image-markup/saveLine"
           data-matrix-index="{{$data->count() > 0 ? $data->last()->row_id : 0}}" data-matrix-rows="0"
           data-matrix-key-value="false">
        <thead>
        <tr>
            @foreach($table as $key => $value)
                <th scope="col" class="text-capitalize">
                    {!! $value !!}
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @if($data->count() > 0)
            @foreach($data as $key => $value)
                <tr class="insert-rows" data-index-row= {{$value->row_id}} >
                    <th class="p-0 align-middle">
                        <div class="form-group">
                            <div class="form-control no-resize"
                                 data-line-status="status-{{$value->row_id}}"></div>
                        </div>
                    </th>
                    <th class="p-0 align-middle">
                        <div class="form-group">
                            <textarea class="form-control no-resize" name="matrix[{{$value->row_id}}][row_id]"
                                      data-key-column="row_id-{{$value->row_id}}">{{$value->row_id}}</textarea>

                            <textarea style="display: none;" class="form-control no-resize coords"
                                      type="hidden"
                                      name="matrix[{{$value->row_id}}][coords]"
                                      data-key-column="coords-{{$value->row_id}}">{{$value->coords}}</textarea>
                        </div>
                    </th>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
