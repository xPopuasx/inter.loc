@if($data->count() > 0)
    @foreach($data as $key => $value)
        <div class="accordion-heading collapsed" id="heading-collapsible-group-item-{{$value->row_id}}" data-bs-toggle="collapse" data-bs-target="#collapse-collapsible-group-item-{{$value->row_id}}" aria-expanded="false" aria-controls="collapse-collapsible-group-item-2">
            <h6 class="btn btn-link btn-group-justified pt-2 pb-2 mb-0 pe-0 ps-0 d-flex align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="small me-2" role="img" fill="currentColor" componentname="orchid-icon">
                    <path d="M8.489 31.975c-0.271 0-0.549-0.107-0.757-0.316-0.417-0.417-0.417-1.098 0-1.515l14.258-14.264-14.050-14.050c-0.417-0.417-0.417-1.098 0-1.515s1.098-0.417 1.515 0l14.807 14.807c0.417 0.417 0.417 1.098 0 1.515l-15.015 15.022c-0.208 0.208-0.486 0.316-0.757 0.316z"></path>
                </svg>
                Разметка этажа ID: {{$value->number}}
            </h6>
        </div>

        <div id="collapse-collapsible-group-item-{{$value->row_id}}" class="mt-2 collapse" aria-labelledby="heading-collapsible-group-item-{{$value->row_id}}" data-bs-parent="#accordion-9ac74e0a608c31b080ff38c517cbb9780cc15eb3" style="">
            <div class="bg-white rounded-top shadow-sm mb-3">
                <fieldset class="mb-4" data-async="">
                    <div class="bg-white rounded shadow-sm p-4 py-4 d-flex flex-column">
                        <div class="form-group">
                            <label for="field-posttitle-6a2629f4e22141742fbef5e4354713127697359a" class="form-label">
                                ID
                                <sup class="text-danger">*</sup>
                            </label>
                            <div data-controller="input" data-input-mask="">
                                <input class="form-control" name="floor[{{$value->row_id}}][row_id]" disabled value="{{$value->row_id}}" type="text" max="255" required="required" title="Название поста" placeholder="Укажите название поста" id="field-posttitle-6a2629f4e22141742fbef5e4354713127697359a">
                            </div>
                            <small class="form-text text-muted">Это поле содержит в себе id (не редактируемое поле) </small>
                        </div>
                        <div class="form-group">
                            <label for="field-posttitle-6a2629f4e22141742fbef5e4354713127697359a" class="form-label">
                                Номер этажа
                                <sup class="text-danger">*</sup>
                            </label>
                            <div data-controller="input" data-input-mask="">
                                    <input class="form-control" name="floor[{{$value->row_id}}][row_id]" value="{{$value->number}}" type="number" title="Number" id="field-number-dac8ff0cce3a2aea43fdc220af5a5198052a4ec0">
                            </div>
                            <small class="form-text text-muted"> </small>
                        </div>
                        <div class="form-group">
                            <label for="field-posttitle-6a2629f4e22141742fbef5e4354713127697359a" class="form-label">
                                Номер этажа
                                <sup class="text-danger">*</sup>
                            </label>
                            <div data-controller="input" data-input-mask="">
                                <input class="form-control" name="floor[{{$value->row_id}}][apartments_in_the_floor]" value="{{$value->apartments_in_the_floor}}" type="number" title="Number" id="field-number-dac8ff0cce3a2aea43fdc220af5a5198052a4ec0">
                                <input type="hidden" class="form-control no-resize" name="matrix[{{$value->row_id}}][coords]" data-key-column="coords-{{$value->row_id}}" value="{{$value->coords}}">
                            </div>
                            <small class="form-text text-muted"> </small>
                        </div>
                        <div class="form-group">
                            <label for="field-textarea-f387baeb8bd3b5a14ca4201c36b3cfdce8e47c86" class="form-label">Изображение для разметки этажа</label>
                            <div data-controller="picture" style="width: 300px;" data-picture-value="" data-picture-storage="public" data-picture-target="url" data-picture="image-{{$value->row_id}}" data-picture-url="{{$value->image_floor}}" data-picture-max-file-size="2" data-picture-accepted-files="image/*" data-picture-groups="">
                                <div class="border-dashed text-end p-3 picture-actions">

                                    <div class="fields-picture-container no-resize"  data-key-column="image_floor-{{$value->row_id}}">
                                        <img src="{{$value->image_floor}}" class="picture-preview img-fluid img-full mb-2 border" alt="">
                                    </div>

                                    <div class="btn-group">
                                        <label class="btn btn-default m-0">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor" id="field-picture-a18c5a3f777fef42fa642bd1dc82b8f5788ccdab" componentname="orchid-icon">
                                                <path d="M23.845 8.124c-1.395-3.701-4.392-6.045-8.921-6.045-5.762 0-9.793 4.279-10.14 9.86-2.778 0.889-4.784 3.723-4.784 6.933 0 3.93 3.089 7.249 6.744 7.249h2.889c0.552 0 1-0.448 1-1s-0.448-1-1-1h-2.889c-2.572 0-4.776-2.404-4.776-5.249 0-2.514 1.763-4.783 3.974-5.163l0.907-0.156-0.080-0.916-0.008-0.011c0-4.871 3.205-8.545 8.161-8.545 3.972 0 6.204 1.957 7.236 5.295l0.214 0.688 0.721 0.015c3.715 0.078 6.972 3.092 6.972 6.837 0 3.408-2.259 7.206-5.678 7.206h-2.285c-0.552 0-1 0.448-1 1s0.448 1 1 1l2.277-0.003c5-0.132 7.605-4.908 7.605-9.203 0-4.616-3.617-8.305-8.14-8.791zM16.75 16.092c-0.006-0.006-0.008-0.011-0.011-0.016l-0.253-0.264c-0.139-0.146-0.323-0.219-0.508-0.218-0.184-0.002-0.368 0.072-0.509 0.218l-0.253 0.264c-0.005 0.005-0.006 0.011-0.011 0.016l-3.61 3.992c-0.28 0.292-0.28 0.764 0 1.058l0.252 0.171c0.28 0.292 0.732 0.197 1.011-0.095l2.128-2.373v10.076c0 0.552 0.448 1 1 1s1-0.448 1-1v-10.066l2.199 2.426c0.279 0.292 0.732 0.387 1.011 0.095l0.252-0.171c0.279-0.293 0.279-0.765 0-1.058z"></path>
                                            </svg>
                                            Загрузить
                                            <input type="file" accept="image/*" data-target="picture.upload" data-action="change->picture#upload" class="d-none">
                                        </label>

                                        <button type="button" class="btn btn-outline-danger picture-remove" data-action="picture#clear">Удалить</button>
                                    </div>

                                    <input type="file" accept="image/*" class="d-none">
                                </div>

                                <input class="picture-path d-none" type="text" data-target="picture.source" target="url" name="picture" title="Picture">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    @endforeach
@endif
    <div class="row bg-light m-0 p-md-4 p-3 border-top rounded-bottom">
        <div class="form-group">
            <table class="matrix table table-bordered" data-controller="matrix" data-remove-url="floors-image-markup/removeLine" data-save-url="floors-image-markup/saveLine" data-matrix-index="{{$data->count() > 0 ? $data->last()->row_id : 0}}" data-matrix-rows="0" data-matrix-key-value="false">
                <thead>
                <tr>
                    @foreach($table as $key => $value)
                    <th scope="col" class="text-capitalize">
                        {!! $value !!}
                    </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @if($data->count() > 0)
                    @foreach($data as $key => $value)
                    <tr class="insert-rows" data-index-row = {{$value->row_id}} >
                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <div class="form-control no-resize" data-line-status="status-{{$value->row_id}}"></div>
                            </div>
                        </th>

                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <textarea class="form-control no-resize" name="matrix[{{$value->row_id}}][row_id]" data-key-column="row_id-{{$value->row_id}}">{{$value->row_id}}</textarea>

                                <textarea style="display: none;" class="form-control no-resize"
                                          name="matrix[{{$value->row_id}}][coords]"
                                          data-key-column="coords-{{$value->row_id}}">{{$value->coords}}</textarea>
                            </div>
                        </th>

                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <input type="hidden" class="form-control no-resize" name="matrix[{{$value->row_id}}][coords]" data-key-column="coords-{{$value->row_id}}" value="{{$value->coords}}">
                            </div>
                        </th>

                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <textarea class="form-control no-resize" name="matrix[{{$value->row_id}}][number]" data-key-column="number-{{$value->row_id}}">{{$value->number}}</textarea>
                            </div>
                        </th>

                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <textarea class="form-control no-resize" name="matrix[{{$value->row_id}}][apartments_in_the_floor]" data-key-column="apartments_in_the_floor-{{$value->row_id}}">{{$value->apartments_in_the_floor}}</textarea>
                            </div>
                        </th>

                        <th class="p-0 align-middle">
                            <div class="form-group">
                                <div data-controller="picture" data-picture-value="" data-picture-storage="public" data-picture-target="url" data-picture="image-{{$value->row_id}}" data-picture-url="{{$value->image_floor}}" data-picture-max-file-size="2" data-picture-accepted-files="image/*" data-picture-groups="">
                                    <div class="border-dashed text-end p-3 picture-actions">

                                        <div class="fields-picture-container no-resize"  data-key-column="image_floor-{{$value->row_id}}">
                                            <img src="{{$value->image_floor}}" class="picture-preview img-fluid img-full mb-2 border" style="width:180px;" alt="">
                                        </div>

                                        <div class="btn-group">
                                            <label class="btn btn-default m-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor" id="field-picture-a18c5a3f777fef42fa642bd1dc82b8f5788ccdab" componentname="orchid-icon">
                                                    <path d="M23.845 8.124c-1.395-3.701-4.392-6.045-8.921-6.045-5.762 0-9.793 4.279-10.14 9.86-2.778 0.889-4.784 3.723-4.784 6.933 0 3.93 3.089 7.249 6.744 7.249h2.889c0.552 0 1-0.448 1-1s-0.448-1-1-1h-2.889c-2.572 0-4.776-2.404-4.776-5.249 0-2.514 1.763-4.783 3.974-5.163l0.907-0.156-0.080-0.916-0.008-0.011c0-4.871 3.205-8.545 8.161-8.545 3.972 0 6.204 1.957 7.236 5.295l0.214 0.688 0.721 0.015c3.715 0.078 6.972 3.092 6.972 6.837 0 3.408-2.259 7.206-5.678 7.206h-2.285c-0.552 0-1 0.448-1 1s0.448 1 1 1l2.277-0.003c5-0.132 7.605-4.908 7.605-9.203 0-4.616-3.617-8.305-8.14-8.791zM16.75 16.092c-0.006-0.006-0.008-0.011-0.011-0.016l-0.253-0.264c-0.139-0.146-0.323-0.219-0.508-0.218-0.184-0.002-0.368 0.072-0.509 0.218l-0.253 0.264c-0.005 0.005-0.006 0.011-0.011 0.016l-3.61 3.992c-0.28 0.292-0.28 0.764 0 1.058l0.252 0.171c0.28 0.292 0.732 0.197 1.011-0.095l2.128-2.373v10.076c0 0.552 0.448 1 1 1s1-0.448 1-1v-10.066l2.199 2.426c0.279 0.292 0.732 0.387 1.011 0.095l0.252-0.171c0.279-0.293 0.279-0.765 0-1.058z"></path>
                                                </svg>
                                                Загрузить
                                                <input type="file" accept="image/*" data-target="picture.upload" data-action="change->picture#upload" class="d-none">
                                            </label>

                                            <button type="button" class="btn btn-outline-danger picture-remove" data-action="picture#clear">Удалить</button>
                                        </div>

                                        <input type="file" accept="image/*" class="d-none">
                                    </div>

                                    <input class="picture-path d-none" type="text" data-target="picture.source" target="url" name="picture" title="Picture">
                                </div>
                            </div>
                        </th>
                        <th class="p-0 align-middle">
                            <ul class="image-markup-settings">
                                <li>
                                    <button class="btn btn-link" type="submit" data-action="save-line" name="matrix[{{$value->row_id}}][save]">
                                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2">
                                            <path d="M16 0c-8.836 0-16 7.163-16 16s7.163 16 16 16c8.837 0 16-7.163 16-16s-7.163-16-16-16zM16 30.032c-7.72 0-14-6.312-14-14.032s6.28-14 14-14 14 6.28 14 14-6.28 14.032-14 14.032zM22.386 10.146l-9.388 9.446-4.228-4.227c-0.39-0.39-1.024-0.39-1.415 0s-0.391 1.023 0 1.414l4.95 4.95c0.39 0.39 1.024 0.39 1.415 0 0.045-0.045 0.084-0.094 0.119-0.145l9.962-10.024c0.39-0.39 0.39-1.024 0-1.415s-1.024-0.39-1.415 0z"></path>
                                        </svg>
                                    </button>
                                </li>
                                <li>
                                    <button class="btn btn-link" type="submit" data-action="remove-line" name="matrix[{{$value->row_id}}][remove]">
                                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2">
                                            <path d="M16 0c-8.836 0-16 7.163-16 16s7.163 16 16 16c8.837 0 16-7.163 16-16s-7.163-16-16-16zM16 30.032c-7.72 0-14-6.312-14-14.032s6.28-14 14-14 14 6.28 14 14-6.28 14.032-14 14.032zM21.657 10.344c-0.39-0.39-1.023-0.39-1.414 0l-4.242 4.242-4.242-4.242c-0.39-0.39-1.024-0.39-1.415 0s-0.39 1.024 0 1.414l4.242 4.242-4.242 4.242c-0.39 0.39-0.39 1.024 0 1.414s1.024 0.39 1.415 0l4.242-4.242 4.242 4.242c0.39 0.39 1.023 0.39 1.414 0s0.39-1.024 0-1.414l-4.242-4.242 4.242-4.242c0.391-0.391 0.391-1.024 0-1.414z"></path>
                                        </svg>
                                    </button>
                                </li>
                            </ul>
                            <a href="#" data-action="matrix#deleteRow" class="small text-muted" title="Remove row" data-delete-id='matrix[{{$value->row_id}}][delete_row]'></a>
                        </th>
                    </tr>
                    @endforeach
                @endif
                <tr class="add-row">
                    <th colspan="5" class="text-center p-0">
                        <a href="#" data-action="matrix#addRow"></a>
                    </th>
                </tr>
                <template>
                    <tr>
                        @foreach($table as $key => $value)

                            @if($key == 'settings')
                            <th class="p-0 align-middle">
                                <ul class="image-markup-settings">
                                    <li>
                                        <button class="btn btn-link" type="submit" data-action="save-line" name="matrix[{index}][save]">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2">
                                                <path d="M16 0c-8.836 0-16 7.163-16 16s7.163 16 16 16c8.837 0 16-7.163 16-16s-7.163-16-16-16zM16 30.032c-7.72 0-14-6.312-14-14.032s6.28-14 14-14 14 6.28 14 14-6.28 14.032-14 14.032zM22.386 10.146l-9.388 9.446-4.228-4.227c-0.39-0.39-1.024-0.39-1.415 0s-0.391 1.023 0 1.414l4.95 4.95c0.39 0.39 1.024 0.39 1.415 0 0.045-0.045 0.084-0.094 0.119-0.145l9.962-10.024c0.39-0.39 0.39-1.024 0-1.415s-1.024-0.39-1.415 0z"></path>
                                            </svg>
                                        </button>
                                    </li>
                                    <li>
                                        <button class="btn btn-link" type="submit" data-action="remove-line" name="matrix[{index}][remove]">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2">
                                                <path d="M16 0c-8.836 0-16 7.163-16 16s7.163 16 16 16c8.837 0 16-7.163 16-16s-7.163-16-16-16zM16 30.032c-7.72 0-14-6.312-14-14.032s6.28-14 14-14 14 6.28 14 14-6.28 14.032-14 14.032zM21.657 10.344c-0.39-0.39-1.023-0.39-1.414 0l-4.242 4.242-4.242-4.242c-0.39-0.39-1.024-0.39-1.415 0s-0.39 1.024 0 1.414l4.242 4.242-4.242 4.242c-0.39 0.39-0.39 1.024 0 1.414s1.024 0.39 1.415 0l4.242-4.242 4.242 4.242c0.39 0.39 1.023 0.39 1.414 0s0.39-1.024 0-1.414l-4.242-4.242 4.242-4.242c0.391-0.391 0.391-1.024 0-1.414z"></path>
                                            </svg>
                                        </button>
                                    </li>
                                </ul>
                                <a href="#" data-action="matrix#deleteRow" class="small text-muted" title="Remove row" data-delete-id='matrix[{index}][delete_row]'></a>
                            </th>
                            @elseif($key == 'image_floor')
                                <th class="p-0 align-middle">
                                    <div class="form-group">
                                        <div data-controller="picture" data-picture-value="" data-picture-storage="public" data-picture-target="url" data-picture="image-{index}" data-picture-url="" data-picture-max-file-size="2" data-picture-accepted-files="image/*" data-picture-groups="">
                                            <div class="border-dashed text-end p-3 picture-actions">

                                                <div class="fields-picture-container no-resize" data-key-column="{{$key}}-{index}">
                                                    <img src="" class="picture-preview img-fluid img-full mb-2 border" style="width:180px;" alt="">
                                                </div>

                                                <div class="btn-group">
                                                    <label class="btn btn-default m-0">
                                                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="1em" height="1em" viewBox="0 0 32 32" class="me-2" role="img" fill="currentColor" id="field-picture-a18c5a3f777fef42fa642bd1dc82b8f5788ccdab" componentname="orchid-icon">
                                                            <path d="M23.845 8.124c-1.395-3.701-4.392-6.045-8.921-6.045-5.762 0-9.793 4.279-10.14 9.86-2.778 0.889-4.784 3.723-4.784 6.933 0 3.93 3.089 7.249 6.744 7.249h2.889c0.552 0 1-0.448 1-1s-0.448-1-1-1h-2.889c-2.572 0-4.776-2.404-4.776-5.249 0-2.514 1.763-4.783 3.974-5.163l0.907-0.156-0.080-0.916-0.008-0.011c0-4.871 3.205-8.545 8.161-8.545 3.972 0 6.204 1.957 7.236 5.295l0.214 0.688 0.721 0.015c3.715 0.078 6.972 3.092 6.972 6.837 0 3.408-2.259 7.206-5.678 7.206h-2.285c-0.552 0-1 0.448-1 1s0.448 1 1 1l2.277-0.003c5-0.132 7.605-4.908 7.605-9.203 0-4.616-3.617-8.305-8.14-8.791zM16.75 16.092c-0.006-0.006-0.008-0.011-0.011-0.016l-0.253-0.264c-0.139-0.146-0.323-0.219-0.508-0.218-0.184-0.002-0.368 0.072-0.509 0.218l-0.253 0.264c-0.005 0.005-0.006 0.011-0.011 0.016l-3.61 3.992c-0.28 0.292-0.28 0.764 0 1.058l0.252 0.171c0.28 0.292 0.732 0.197 1.011-0.095l2.128-2.373v10.076c0 0.552 0.448 1 1 1s1-0.448 1-1v-10.066l2.199 2.426c0.279 0.292 0.732 0.387 1.011 0.095l0.252-0.171c0.279-0.293 0.279-0.765 0-1.058z"></path>
                                                        </svg>
                                                        Загрузить
                                                        <input type="file" accept="image/*" data-target="picture.upload" data-action="change->picture#upload" class="d-none">
                                                    </label>

                                                    <button type="button" class="btn btn-outline-danger picture-remove" data-action="picture#clear">Удалить</button>
                                                </div>

                                                <input type="file" accept="image/*" class="d-none">
                                            </div>

                                            <input class="picture-path d-none" type="text" data-target="picture.source" target="url" name="picture" title="Picture">
                                        </div>
                                    </div>
                                </th>

                            @elseif($key == 'status')
                                <th class="p-0 align-middle">
                                    <div class="form-group">
                                        <div class="form-control no-resize" data-line-status="status-{index}"></div>
                                    </div>
                                </th>
                            @else
                            <th class="p-0 align-middle">
                                <div class="form-group">
                                    <textarea class="form-control no-resize" name="matrix[{index}][{{$key}}]" data-key-column="{{$key}}-{index}"></textarea>
                                </div>
                            </th>
                            @endif
                        @endforeach
                    </tr>
                </template>
                </tbody>
            </table>

        </div>
    </div>
</div>
<div class="bg-white rounded-top shadow-sm mb-3">
    <div class="row g-0">
        <div class="col col-lg-12 mt-6 p-4">
            <div class="mt-2 text-dark fw-light">
                <div class="container-image">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="map_swg" style="width: 100%;" >

                    </svg>
                    <img src="{{$image}}" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
</div>
<canvas id="overlay"></canvas>
