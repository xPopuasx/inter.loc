export const initializeCanvas = () => {
    const canvas = document.getElementById('overlay');
    const image = document.querySelector('.container-image img');
    const issetApartments = document.querySelectorAll('.coords');

    if (canvas) {
        canvas.width = image.width;
        canvas.height = image.height;

        const ctx = canvas.getContext('2d');

        let points = []; // Точки текущей фигуры
        let apartments = []; // Массив квартир (каждая квартира — это массив точек)
        let mousePos = null; // Позиция мыши

        issetApartments.forEach(textarea => {
            apartments.push(JSON.parse(textarea.value));
        });

        // Обработчик кликов для установки точек
        canvas.addEventListener('click', (event) => {
            const rect = canvas.getBoundingClientRect();
            const x = event.clientX - rect.left;
            const y = event.clientY - rect.top;

            points.push({x, y});
            drawPoint(x, y);
            drawLines(points);
        });

        // Обработчик движения мыши
        canvas.addEventListener('mousemove', (event) => {
            const rect = canvas.getBoundingClientRect();
            const x = event.clientX - rect.left;
            const y = event.clientY - rect.top;
            mousePos = {x, y};
            redraw(); // Перерисовываем с учетом пунктира
        });

        // Обработчик нажатия клавиш
        document.addEventListener('keydown', (event) => {
            if (event.key === 'Enter') {
                finishApartment(); // Завершить текущую фигуру
            } else if (event.ctrlKey && event.keyCode === 90 && !event.shiftKey) {
                clearLastPoint(); // Удалить последнюю точку
            } else if (event.ctrlKey && event.shiftKey && event.keyCode === 90) {
                clearLastFigure(); // Восстановить последнюю точку (пример действия)
            }
        });

        // Рисование пунктирной линии
        function drawDashedLine(start, end) {
            if (!start || !end) return;

            ctx.setLineDash([5, 5]); // Устанавливаем стиль линии как пунктир
            ctx.strokeStyle = '#00FFFF';
            ctx.lineWidth = 2;

            ctx.beginPath();
            ctx.moveTo(start.x, start.y);
            ctx.lineTo(end.x, end.y);
            ctx.stroke();

            ctx.setLineDash([]); // Сбрасываем стиль линии
        }

        // Функция для рисования точки
        function drawPoint(x, y) {
            ctx.fillStyle = '#00FFFF';
            ctx.beginPath();
            ctx.arc(x, y, 5, 0, 2 * Math.PI);
            ctx.fill();
        }

        // Функция для рисования линий между точками
        function drawLines(points) {
            if (points.length < 2) return;

            ctx.strokeStyle = '#66FFBA';
            ctx.lineWidth = 4;
            ctx.beginPath();
            ctx.moveTo(points[0].x, points[0].y);

            for (let i = 1; i < points.length; i++) {
                ctx.lineTo(points[i].x, points[i].y);
            }
            ctx.stroke();
        }

        // Завершение текущей квартиры
        function finishApartment() {
            if (points.length < 3) {
                alert('В полигоне, для создания фигуры, должно быть не меньше 3 точек');
                return;
            }

            // Замыкаем полигон
            ctx.strokeStyle = '#00FFFF';
            ctx.beginPath();
            ctx.moveTo(points[0].x, points[0].y);
            points.forEach(point => ctx.lineTo(point.x, point.y));
            ctx.closePath();
            ctx.stroke();

            // Сохраняем квартиру и очищаем текущие точки
            apartments.push([...points]);
            points = [];
            mousePos = null; // Сбрасываем положение мыши
            redraw();
        }

        // Очистка последней точки
        function clearLastPoint() {
            if (points.length > 0) {
                points.pop();
                redraw();
            }
        }

        // Очистка последней фигуры
        function clearLastFigure() {
            if (apartments.length > 0) {
                apartments.pop();
                redraw();
            }
        }

        // Полная отчистка
        function clearAll() {
            points = [];
            apartments = [];
            redraw();
        }

        // Перерисовка всего
        function redraw() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            // Перерисовываем все квартиры
            apartments.forEach(apartment => {
                drawLines(apartment);
                ctx.beginPath();
                ctx.moveTo(apartment[0].x, apartment[0].y);
                apartment.forEach(point => ctx.lineTo(point.x, point.y));
                ctx.closePath();
                ctx.strokeStyle = '#00FFFF';
                ctx.stroke();
            });

            // Перерисовываем текущие точки и линии
            drawLines(points);
            points.forEach(point => drawPoint(point.x, point.y));

            // Рисуем пунктирную линию, если есть последняя точка и текущая позиция мыши
            if (points.length > 0 && mousePos) {
                drawDashedLine(points[points.length - 1], mousePos);
            }
        }

        // Привязка кнопок интерфейса
        document.getElementById('finishApartment').addEventListener('click', finishApartment);
        document.getElementById('clearLastPoint').addEventListener('click', clearLastPoint);
        document.getElementById('clearLastFigure').addEventListener('click', clearLastFigure);
        document.getElementById('clearAll').addEventListener('click', clearAll);
    }
};






