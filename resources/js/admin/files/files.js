import {initRemoveButtons} from "./init";


document.addEventListener("turbo:load", () => {
    let buttonSelector = document.querySelector('.btn-remove')
    if(buttonSelector){
        initRemoveButtons.setButtons(buttonSelector)
    }
})


window.addEventListener('load', () => {
    let buttonSelector = document.querySelector('.btn-remove')

    if(buttonSelector) {
        initRemoveButtons.setButtons(buttonSelector)
    }
});
