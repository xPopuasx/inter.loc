const formData = {
    '_token': document.querySelector("meta[name='csrf_token']").getAttribute('content')
};

export const initRemoveButtons = {
    setButtons: function (buttonSelector){
        Object(buttonSelector).addEventListener('click', e => {

            postDataSettings.setFileId(e.target.parentNode.getAttribute('data-file-id'))

            postDataSettings.postDeleteFile('/admin/deleteFile');
        });
    }
}

const postDataSettings = {
    setFileId: function (fileId){
        formData.file_id = fileId
    },

    postDeleteFile: function (deleteUrl) {
        $.post(deleteUrl, formData, function(response) {
            return true;
        }).catch((e) => {
            let error = e.responseJSON.error

            Swal.fire({
                title: 'Ошибка',
                html: error,
                icon: 'error',
                confirmButtonText: 'Понял',
                confirmButtonColor: '#000',
                width: '500px',
            })
        });
    }
}
