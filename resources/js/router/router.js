import VueRouter from 'vue-router';

import IndexComponent from "../components/IndexComponent";
import AboutComponent from "../components/AboutComponent";
import ContactsComponent from "../components/ContactsComponent";
import HouseComponent from "../components/HouseComponent";
import FlatsComponent from "../components/FlatComponent";
import HousePageComponent from "../components/HousePageComponent";
import FloorPageComponent from "../components/FloorPageComponent";
import FlatPageComponent from "../components/FlatPageComponent";
import WishlistComponent from "../components/WishlistComponent";
import PostComponent from "../components/PostComponent.vue";

export default new VueRouter({
    routes: [
        {
            path: '/',
            component: IndexComponent,
            name: 'index',
            meta: {
                isSingle: true,
                title: 'Главная',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Главная'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Главная'
                    }
                ]
            }
        },
        {
            path: '/about',
            component: AboutComponent,
            name: 'about',
            meta: {
                isSingle: true,
                title: 'О компании',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| О нас'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| О нас'
                    }
                ]
            }
        },
        {
            path: '/news',
            component: PostComponent,
            name: 'news',
            meta: {
                isSingle: true,
                title: 'Новости',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| О Новости'
                    }
                ]
            }
        },
        {
            path: '/contacts',
            component: ContactsComponent,
            name: 'contacts',
            meta: {
                isSingle: true,
                title: 'Контакты',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Контакты'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Контакты'
                    }
                ]
            }
        },
        {
            path: '/zhilaya_nedvizhimost',
            component: HouseComponent,
            name: 'house',
            meta: {
                isSingle: true,
                title: 'Проекты',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Жилая недвижимость'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Жилая недвижимость'
                    }
                ]
            }
        },
        {
            path: '/flats',
            component: FlatsComponent,
            name: 'flats',
            meta: {
                isSingle: true,
                title: 'Квартиры',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Объекты'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Объекты'
                    }
                ]
            }
        },
        {
            path: '/zhilaya-nedvizhesot/:id',
            component: HousePageComponent,
            name: 'house-page',
            meta: {
                title: 'Фид',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Объекты'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Объекты'
                    }
                ]
            }
        },
        {
            path: '/zhilaya-nedvizhemost/:id/etazh/:floorId',
            component: FloorPageComponent,
            name: 'floor-page',
            meta: {
                title: 'Фид',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Объекты'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Объекты'
                    }
                ]
            }
        },
        {
            path: '/zhilaya-nedvizhemost/:id/etazh/:floorId/product/:flatId',
            component: FlatPageComponent,
            name: 'flat-page',
            meta: {
                title: 'Квартира',
                metaTags: [
                    {
                        name: 'description',
                        content: 'ООО интер| Квартиры'
                    },
                    {
                        property: 'og:description',
                        content: 'ООО интер| Квартиры'
                    }
                ]
            }
        },
        {
            path: '/wishlist',
            component: WishlistComponent,
            name: 'wishlist',
            meta: {
                title: 'Избранное',
            }
        },
    ],
    mode: 'history'
})
