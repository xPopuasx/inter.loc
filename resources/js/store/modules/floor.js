import axios from "axios";

export default {
    strict: true,
    namespaced: true,
    state: {
        floorLoader: false,
        floors: [],
        floor: {},
    },
    mutations: {
        ['SET_FLOOR_LOADER']: (state, floorLoader) => {
            state.floorLoader = floorLoader;
        },
        ['SET_FLOORS']: (state, floors) => {
            state.floors = floors;
        },
        ['SET_FLOOR']: (state, floor) => {
            state.floor = floor;
        },
        ['SET_DEFAULT_FLOOR']: (state) => {
            state.floor = {};
        },
    },
    actions: {
        ['QUERY_GET_FLOORS']: async ({commit, dispatch}, payload) => {
            commit('SET_FLOOR_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses/${payload.projectId}/floors`,{});

                commit('SET_FLOORS', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_FLOOR_LOADER', false);
        },
        ['QUERY_GET_FLOOR']: async ({commit, dispatch}, payload) => {
            commit('SET_FLOOR_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses/${payload.projectId}/floors/${payload.floorId}`,{});

                commit('SET_FLOOR', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_FLOOR_LOADER', false);
        },
    },
    getters: {
    },
};
