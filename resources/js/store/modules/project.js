import axios from "axios";

export default {
    strict: true,
    namespaced: true,
    state: {
        projectLoader: false,
        projects: [],
        project: {
            floors: [],
            information: {}
        },
    },
    mutations: {
        ['SET_PROJECT_LOADER']: (state, projectLoader) => {
            state.projectLoader = projectLoader;
        },
        ['SET_PROJECTS']: (state, projects) => {
            state.projects = projects;
        },
        ['SET_PROJECT']: (state, project) => {
            state.project = project;
        },
        ['SET_DEFAULT_PROJECT']: (state) => {
            state.project = {
                information: {}
            };
        },
    },
    actions: {
        ['QUERY_GET_PROJECTS']: async ({commit, dispatch}) => {
            commit('SET_PROJECT_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses`,{});

                commit('SET_PROJECTS', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_PROJECT_LOADER', false);
        },
        ['QUERY_GET_PROJECT']: async ({commit, dispatch}, payload) => {
            commit('SET_PROJECT_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses/${payload.id}`,{});

                commit('SET_PROJECT', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_PROJECT_LOADER', false);
        },
    },
    getters: {
    },
};
