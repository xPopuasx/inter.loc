import axios from "axios";

export default {
    strict: true,
    namespaced: true,
    state: {
        slideLoader: false,
        slides: [],
    },
    mutations: {
        ['SET_SLIDES']: (state, slides) => {
            state.slides = slides;
        },
        ['SET_SLIDE_LOADER']: (state, flatLoader) => {
            state.slideLoader = flatLoader;
        },
    },
    actions: {
        ['QUERY_GET_SLIDES']: async ({commit, dispatch}) => {
            commit('SET_SLIDE_LOADER', true);

            try {
                const {data} = await axios.get(`/api/pages/main/slides`,{});

                commit('SET_SLIDES', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_SLIDE_LOADER', false);
        },
    },
    getters: {
    },
};
