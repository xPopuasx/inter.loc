import axios from "axios";
import pagination from "../../components/helpers/Pagination";
import router from "../../router/router";

export default {
    strict: true,
    namespaced: true,
    state: {
        flatLoader: false,
        flats: [],
        flat: {},
        filters: [],
        tags: [],
        pagination: {
            page: 1,
            total: 0,
            perPage: 15,
            lastPage: 15,
            currentPage: 1,
        },
    },
    mutations: {
        ['SET_FLAT_LOADER']: (state, flatLoader) => {
            state.flatLoader = flatLoader;
        },
        ['SET_FLATS']: (state, flats) => {
            state.flats = flats;
        },
        ['SET_FILTERS']: (state, filters) => {
            state.filters = filters;
        },
        ['SET_FLAT']: (state, flat) => {
            state.flat = flat;
        },
        ['SET_DEFAULT_FLAT']: (state) => {
            state.flat = {};
        },
        ['SET_PAGINATION']: (state, pagination) => {
            state.pagination = pagination;
        },
        ['SET_TAGS']: (state, tags) => {
            state.tags = tags;
        },
    },
    actions: {
        ['QUERY_GET_FLATS']: async ({commit, dispatch}, payload) => {
            commit('SET_FLAT_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses/${payload.projectId}/floors/${payload.floorId}/flats`, {});

                commit('SET_FLAT', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_FLAT_LOADER', false);
        },
        ['QUERY_GET_FLAT']: async ({commit, dispatch}, payload) => {
            commit('SET_FLAT_LOADER', true);

            try {
                const {data} = await axios.get(`/api/houses/${payload.projectId}/floors/${payload.floorId}/flats/${payload.flatId}`, {});

                commit('SET_FLAT', data.data);
            } catch (err) {
                console.log(err)
            }

            commit('SET_FLAT_LOADER', false);
        },
        ['QUERY_GET_ALL_FLAT']: async ({commit, dispatch}, payload) => {
            commit('SET_FLAT_LOADER', true);

            let params = window.location.search.replace( '?', '');

            try {
                const {data} = await axios.get(`/api/flats?${params}`);

                commit('SET_FLATS', data.data);

                commit('SET_PAGINATION', {
                    total: data.meta.total,
                    perPage: data.meta.per_page,
                    lastPage: data.meta.last_page,
                    currentPage: data.meta.current_page,
                });

            } catch (err) {
                console.log(err)
            }

            commit('SET_FLAT_LOADER', false);
        },
        ['QUERY_GET_FILTERS']: async ({commit, dispatch}, payload) => {
            commit('SET_FLAT_LOADER', true);
                try {
                    const {data} = await axios.get(`/api/flats/filters`, {});
                    commit('SET_FILTERS', data.filters);
                    commit('SET_TAGS', data.tags);

                } catch (err) {
                    console.log(err)
                }
                commit('SET_FLAT_LOADER', false);
        },
    },
    getters: {},
};
