import axios from "axios";

export default {
    strict: true,
    namespaced: true,
    state: {
        wishlist: []
    },
    mutations: {
        ['SET_WISHLIST']: (state, wishlist) => {
            state.wishlist = wishlist;
        },
        ['SET_WISHLIST_ITEM']: (state, wishlist) => {
            state.wishlist = wishlist;
        },
    },
    actions: {
        ['QUERY_SET_WISHLIST']: async ({commit, dispatch}) => {
            try {
                const {data} = await axios.get(`/api/wishlist`,{});
                commit('SET_WISHLIST', data);
            } catch (err) {
                console.log(err)
            }
        },
        ['QUERY_SET_WISHLIST_ITEM']: async ({commit, dispatch}, payload) => {
            try {
                const {data} = await axios.post(`/api/wishlist/items`,{
                    flat_id : payload.flatId
                });

                commit('SET_WISHLIST', data);
            } catch (err) {
                console.log(err)
            }
        },
        ['QUERY_REMOVE_WISHLIST_ITEM']: async ({commit, dispatch}, payload) => {
            try {
                const {data} = await axios.delete(`/api/wishlist/items?flat_id=${payload.flatId}`);

                commit('SET_WISHLIST', data);
            } catch (err) {
                console.log(err)
            }
        },
    },
    getters: {
    },
};
