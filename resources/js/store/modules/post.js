import axios from "axios";

export default {
    strict: true,
    namespaced: true,
    state: {
        loader: false,
        posts: [],
        tags: [],
        pagination: {
            page: 1,
            total: 0,
            perPage: 15,
            lastPage: 15,
            currentPage: 1,
        },
    },
    mutations: {
        ['SET_POSTS']: (state, posts) => {
            state.posts = posts;
        },
        ['SET_PAGINATION']: (state, pagination) => {
            state.pagination = pagination;
        },
        ['SET_LOADER']: (state, loader) => {
            state.loader = loader;
        },
        ['SET_TAGS']: (state, tags) => {
            state.tags = tags;
        },
    },
    actions: {
        ['QUERY_GET_POSTS']: async ({commit, dispatch}, payload) => {
            commit('SET_LOADER', true);

            let params = window.location.search.replace( '?', '');

            try {
                const {data} = await axios.get(`/api/posts?${params}`);

                commit('SET_POSTS', data.data);

                commit('SET_PAGINATION', {
                    total: data.meta.total,
                    perPage: data.meta.per_page,
                    lastPage: data.meta.last_page,
                    currentPage: data.meta.current_page,
                });

            } catch (err) {
                console.log(err)
            }

            commit('SET_LOADER', false);
        },
        ['QUERY_GET_FILTERS']: async ({commit, dispatch}, payload) => {
            commit('SET_LOADER', true);
            try {
                const {data} = await axios.get(`/api/posts/filters`, {});
                commit('SET_TAGS', data.tags);

            } catch (err) {
                console.log(err)
            }
            commit('SET_LOADER', false);
        },
    },
    getters: {
    },
};
