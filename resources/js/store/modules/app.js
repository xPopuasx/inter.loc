export default {
    strict: true,
    namespaced: true,
    state: {
        loader: false,
    },
    mutations: {
        ['SET_LOADER']: (state, loader) => {
            state.loader = loader;
        },
    },
    actions: {

    },
    getters: {
    },
};
