import Vue from 'vue'
import Vuex from 'vuex'

import App from './modules/app'
import Project from './modules/project'
import Floor from './modules/floor'
import Flat from './modules/flat'
import Slider from "./modules/Banner/slider";
import Wishlist from "./modules/wishlist";
import Posts from './modules/post'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        App,
        Project,
        Floor,
        Flat,
        Slider,
        Wishlist,
        Posts
    }
})
