/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import App from './components/App';
import VueRouter from 'vue-router';
import YmapPlugin from 'vue-yandex-maps';
import money from 'v-money';
import VueTheMask from 'vue-the-mask';
import VueCarousel from 'vue-carousel';
import VueScrollReveal from 'vue-scroll-reveal';

const settings = {
    apiKey: '',
    lang: 'ru_RU',
    coordorder: 'latlong',
    enterprise: false,
    version: '2.1'
}

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


// Модули
Vue.component('loader-module', require('./modules/LoaderModule.vue').default);
Vue.component('menu-module', require('./modules/MenuModule.vue').default);
Vue.component('footer-module', require('./modules/FooterModule.vue').default);
Vue.component('feedback-module', require('./components/form/module/FeedbackComponent').default);
Vue.component('banner-module', require('./modules/BannerModule.vue').default);
Vue.component('project-grid-module', require('./modules/ProjectGridModule.vue').default);
Vue.component('project-grid-map-module', require('./modules/ProjectGridMapModule').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import router from "./router/router";
import store from "./store"

Vue.use(YmapPlugin, settings);
Vue.use(VueRouter);
Vue.use(VueTheMask)
Vue.use(money, {precision: 4})
Vue.use(VueCarousel);
Vue.use(VueScrollReveal, {
    class: 'v-scroll-reveal', // A CSS class applied to elements with the v-scroll-reveal directive; useful for animation overrides.
    duration: 1000,
    scale: 1,
    distance: '100px',
    mobile: false
});


Vue.config.productionTip = false

const app = new Vue({
    el:'#app',
    render: h => h(App),
    router,
    store
});

if(store.state.Wishlist.wishlist.length === 0){
    store.dispatch('Wishlist/QUERY_SET_WISHLIST')
}

router.beforeEach(async function (to, from, next) {
    const menuButton = document.getElementById('menu');

    if(menuButton.getAttribute("aria-expanded") === 'true'){
        menuButton.click()
    }

    document.title = to.meta.title;

    next()
});
