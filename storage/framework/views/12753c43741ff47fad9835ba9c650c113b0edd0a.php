<div id="accordion-<?php echo e($templateSlug); ?>" class="accordion mb-3">
    <?php $__currentLoopData = $manyForms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name => $forms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="accordion-heading <?php if($loop->index): ?> collapsed <?php endif; ?>"
             id="heading-<?php echo e(\Illuminate\Support\Str::slug($name)); ?>"
             data-bs-toggle="collapse"
             data-bs-target="#collapse-<?php echo e(\Illuminate\Support\Str::slug($name)); ?>"
             aria-expanded="true"
             aria-controls="collapse-<?php echo e(\Illuminate\Support\Str::slug($name)); ?>">
            <h6 class="btn btn-link btn-group-justified pt-2 pb-2 mb-0 pe-0 ps-0 d-flex align-items-center">
                <?php if (isset($component)) { $__componentOriginald36eae2be856e5ea3de02a2f65da5a3c27957ebc = $component; } ?>
<?php $component = $__env->getContainer()->make(Orchid\Icons\IconComponent::class, ['path' => 'arrow-right','class' => 'small me-2']); ?>
<?php $component->withName('orchid-icon'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginald36eae2be856e5ea3de02a2f65da5a3c27957ebc)): ?>
<?php $component = $__componentOriginald36eae2be856e5ea3de02a2f65da5a3c27957ebc; ?>
<?php unset($__componentOriginald36eae2be856e5ea3de02a2f65da5a3c27957ebc); ?>
<?php endif; ?> <?php echo $name; ?>

            </h6>
        </div>

        <div id="collapse-<?php echo e(\Illuminate\Support\Str::slug($name)); ?>"
             class="mt-2 collapse <?php if(!$loop->index): ?> show <?php endif; ?>"
             aria-labelledby="heading-<?php echo e(\Illuminate\Support\Str::slug($name)); ?>"
             data-bs-parent="#accordion-<?php echo e($templateSlug); ?>">
                <?php $__currentLoopData = $forms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo $form; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php /**PATH C:\OpenServer\domains\inter.loc\vendor\orchid\platform\resources\views/layouts/accordion.blade.php ENDPATH**/ ?>