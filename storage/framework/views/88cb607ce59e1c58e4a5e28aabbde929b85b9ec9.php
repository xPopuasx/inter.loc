<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="<?php echo e(mix('/js/app.js')); ?>"></script>
        <title>Laravel</title>
    </head>
    <body>
       <div id="app">
           <example-component></example-component>
       </div>
    </body>
</html>
<?php /**PATH C:\OpenServer\domains\inter.loc\resources\views/welcome.blade.php ENDPATH**/ ?>