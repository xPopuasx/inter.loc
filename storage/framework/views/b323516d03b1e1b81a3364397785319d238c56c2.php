<?php $__env->startComponent($typeForm, get_defined_vars()); ?>
    <div class="form-check">
        <input id="<?php echo e($id); ?>" <?php echo e($attributes); ?>>
        <label class="form-check-label" for="<?php echo e($id); ?>"><?php echo e($placeholder ?? ''); ?></label>
    </div>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH C:\OpenServer\domains\inter.loc\vendor\orchid\platform\resources\views/fields/radio.blade.php ENDPATH**/ ?>