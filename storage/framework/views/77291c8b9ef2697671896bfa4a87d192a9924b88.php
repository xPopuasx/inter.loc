<div
     data-controller="chart"
     data-chart-parent="#<?php echo e($slug); ?>"
     data-chart-title="<?php echo e($title); ?>"
     data-chart-labels="<?php echo e($labels); ?>"
     data-chart-datasets="<?php echo e($data); ?>"
     data-chart-type="<?php echo e($type); ?>"
     data-chart-height="<?php echo e($height); ?>"
     data-chart-colors="<?php echo e($colors); ?>"
     data-chart-max-slices="<?php echo e($maxSlices); ?>"
     data-chart-values-over-points="<?php echo e($valuesOverPoints); ?>"
     data-chart-axis-options="<?php echo e($axisOptions); ?>"
     data-chart-bar-options="<?php echo e($barOptions); ?>"
     data-chart-line-options="<?php echo e($lineOptions); ?>"
     data-chart-markers="<?php echo e($markers); ?>"
>
    <div class="bg-white rounded shadow-sm mb-3 pt-3">
        <div class="position-relative w-100">
            <?php if($export): ?>
                <div class="top-right pt-1 pe-4" style="z-index: 1">
                    <button class="btn btn-sm btn-link"
                            data-action="chart#export">
                        <?php echo e(__('Export')); ?>

                    </button>
                </div>
            <?php endif; ?>
            <figure id="<?php echo e($slug); ?>" class="w-100 h-full"></figure>
        </div>
    </div>
</div>
<?php /**PATH C:\OpenServer\domains\inter\vendor\orchid\platform\resources\views/layouts/chart.blade.php ENDPATH**/ ?>