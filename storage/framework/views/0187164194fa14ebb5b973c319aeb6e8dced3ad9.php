<div class="row form-group <?php echo e($align); ?>">
    <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="<?php echo e($class); ?> <?php if(!$loop->last): ?> pe-md-0 <?php endif; ?>">
            <?php echo $field; ?>

        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php /**PATH C:\OpenServer\domains\inter\vendor\orchid\platform\resources\views/fields/group.blade.php ENDPATH**/ ?>