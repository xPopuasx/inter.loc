<?php

declare(strict_types=1);

namespace App\Orchid;

use App\Models\Feedback;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            Menu::make('Новости')
                ->icon('info')
                ->title('Управление сайтом')
                ->list([
                    Menu::make('Список новостей')
                        ->icon('number-list')
                        ->route('platform.posts.list'),
                    Menu::make('Добавить новость')
                        ->icon('plus')
                        ->route('platform.posts.create'),
                ]),

            Menu::make('Документы')
                ->icon('folder')
                ->list([
                    Menu::make('Список документов')
                        ->icon('number-list')
                        ->route('platform.documents.list'),
                    Menu::make('Добавить документ')
                        ->icon('plus')
                        ->route('platform.documents.create'),
                ]),

            Menu::make('Объекты строительства')
                ->icon('fa.building')
                ->list([
                    Menu::make('Список проектов')
                        ->icon('book-open')
                        ->route('platform.houses.list'),
                    Menu::make('Список этажей')
                        ->icon('number-list')
                        ->route('platform.houses.floors.list'),
                    Menu::make('Список квартир')
                        ->icon('notebook')
                        ->route('platform.houses.flats.list'),
                    Menu::make('Добавить проект')
                        ->icon('plus')
                        ->route('platform.houses.create'),
                ]),

            Menu::make('Обращения')
                ->icon('envelope-letter')
                ->title('Взаимодействе с сайтом')
                ->route('platform.feedbacks.list')
                ->badge(function () {
                    return Feedback::all()->where('status', 'create')->count();
                }),

            Menu::make('Страницы')
                ->icon('code')
                ->title('Управление статичными страницами')
                ->list([
                    Menu::make('Главная страница')
                        ->icon('home')
                        ->list([
                            Menu::make('Слайдер')
                                ->icon('picture')
                                ->route('platform.pages.main.slides.list'),
                        ]),
                ]),

            Menu::make('Тэги')
                ->icon('code')
                ->title('Вспомогательный раздел')
                ->list([
                    Menu::make('Список тэгов')
                        ->icon('number-list')
                        ->route('platform.tags.list'),
                    Menu::make('Добавить тэг')
                        ->icon('plus')
                        ->route('platform.tags.create'),
                ]),

            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
