<?php

namespace App\Orchid\Screens\Posts;

use App\Http\Requests\SavePostRequest;
use App\Models\Post;
use App\Models\Tag;
use App\Orchid\Layouts\Posts\PostsEditLayout;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PostsEditScreen extends Screen
{
    public $post;

    public $tags;
    /**
     * Query data.
     *
     * @return array
     */
    public function query(Post $post): iterable
    {
        return [
            'post' => $post,
            'tags' => Tag::query()->where('type', Tag::POST)->get()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->post->exists) ? 'Редактирование новости' : 'Добавление новости';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->post->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block([
                PostsEditLayout::class,
            ])
            ->title('Новости')
            ->description('Данный раздел позволяет наполнять раздел новостей на сайте'),

            Layout::rows([
                Quill::make('post.description')
                    ->title('Описание поста')
                    ->required(),

                Input::make('post.score')
                    ->type('text')
                    ->max(255)
                    ->title(__('Источник'))
                    ->placeholder('Укажите ссылку на источник если таковой имеется')
                    ->help(__('Это поле используется для сохранения авторских прав')),

                Upload::make('post.attachment')
                    ->title('Файлы для загрузки')
                    ->maxFiles(5)
                    ->withoutFormType()
                    ->value($this->post->attachment()->where('group', Tag::POST)->get())
                    ->groups(Tag::POST)
            ]),
        ];
    }

    public function save(SavePostRequest $request, Post $post): RedirectResponse
    {
        $user = Auth::user();

        $attachments = $request->input('post.attachment') ?? [];
        $tags = $request->input('post.tags') ?? [];

        try {
            $data = $request->input('post');
            $data['slug'] = Str::slug($data['title']);
            $data['user_id'] = $user->id;
            $data['status_id'] = 4;
            unset($data['attachment']);
            unset($data['tags']);

            /** @var Post $newPost */
            $newPost = Post::query()->updateOrCreate($data, ['id' => $post->id]);

            $newPost->attachment()->sync($attachments);
            $newPost->tags()->sync($tags ?? []);

            Toast::info($post->exists ? 'Новость успешно изменена' : 'Новость успешно сохранена');

        } catch (Exception $e){
            Attachment::query()->whereIn('id', $attachments)->delete();
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.posts.list');
    }

    public function remove(Post $post)
    {
        $post->delete();

        Toast::info('Новость успешно Удалена');

        return redirect()->route('platform.posts.list');
    }
}
