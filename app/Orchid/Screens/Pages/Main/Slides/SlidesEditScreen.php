<?php

namespace App\Orchid\Screens\Pages\Main\Slides;

use App\Http\Requests\Pages\Main\Slides\SaveSlideRequest;
use App\Http\Requests\SavePostRequest;
use App\Models\Pages\Main\Slide;
use App\Models\Post;
use App\Models\Tag;
use App\Orchid\Layouts\Pages\Main\Slides\SlidesEditLayout;
use App\Orchid\Layouts\Posts\PostsEditLayout;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SlidesEditScreen extends Screen
{
    public $slide;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Slide $slide): iterable
    {
        return [
            'slide' => $slide,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->slide->exists) ? 'Редактирование слайда' : 'Добавление слайда';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->slide->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            SlidesEditLayout::class
        ];
    }

    public function save(SaveSlideRequest $request, Slide $slide): RedirectResponse
    {
        $slide->exists
            ? $slide->update($request->validated()['slide'])
            : Slide::query()->create($request->validated()['slide']);

        Toast::info($slide->exists ? 'Слайд успешно изменён' : 'Слайд успешно сохранён');

        return redirect()->route('platform.pages.main.slides.list');
    }
}
