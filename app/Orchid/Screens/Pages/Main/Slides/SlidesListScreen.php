<?php

namespace App\Orchid\Screens\Pages\Main\Slides;

use App\Models\Pages\Main\Slide;
use App\Models\Post;
use App\Orchid\Layouts\Pages\Main\Slides\SlidesListLayout;
use App\Orchid\Layouts\Posts\PostsListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class SlidesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'slides' => Slide::filters()->defaultSort('id', 'desc')->paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список слайдов на главной странице';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить')
                ->icon('plus')
                ->href(route('platform.pages.main.slides.create')),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            SlidesListLayout::class,
        ];
    }


}
