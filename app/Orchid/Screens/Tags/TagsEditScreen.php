<?php

namespace App\Orchid\Screens\Tags;

use App\Http\Requests\SaveTagRequest;
use App\Models\Tag;
use App\Orchid\Layouts\Tags\TagsEditLayout;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TagsEditScreen extends Screen
{

    public $tag;

    /**
     * @param Tag $tag
     * @return iterable
     */
    public function query(Tag $tag): iterable
    {
        return [
            'tag' => $tag
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->tag->exists) ? 'Редактирование тэга' : 'Добавление тэга';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->tag->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block([
                TagsEditLayout::class,
            ])
            ->title('Тэг')
            ->description('Тэги необходимы для создания новостей загрузки документов и других пометок, это сильно упростит пользоваться сайтом')
        ];
    }

    public function save(SaveTagRequest $request, Tag $tag) : RedirectResponse
    {
        $user = Auth::user();

        try{
            $data = $request->input('tag');

            $data['user_id'] = $user->id;

            $tag->exists
                ? $tag->update($data)
                : Tag::query()->create($data);

            Toast::info($tag->exists ? 'Тэг успешно изменён' : 'Тэг успешно сохранён');

        } catch (Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.tags.list');
    }

    public function remove(Tag $tag): RedirectResponse
    {
        $tag->delete();

        Toast::info(__('Тэг успешно удалён'));

        return redirect()->route('platform.tags.list');
    }
}
