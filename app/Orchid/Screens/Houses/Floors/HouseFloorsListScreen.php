<?php

namespace App\Orchid\Screens\Houses\Floors;

use App\Models\Floor;
use App\Orchid\Layouts\Hoses\Floors\FloorsListLayout;
use Orchid\Screen\Screen;

class HouseFloorsListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'floors' => Floor::filters()->defaultSort('id', 'desc')->paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список этажей';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            FloorsListLayout::class
        ];
    }
}
