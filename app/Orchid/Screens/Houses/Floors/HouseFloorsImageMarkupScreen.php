<?php

namespace App\Orchid\Screens\Houses\Floors;

use App\Http\Requests\RemoveImageMarkupRequest;
use App\Http\Requests\SaveFloorImageMarkupRequest;
use App\Models\Floor;
use App\Models\House;
use App\Services\HouseImageMarkupService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class HouseFloorsImageMarkupScreen extends Screen
{
    public $house;
    public $image;
    public $table;
    public $data;

    /**
     * Query data.
     *
     * @param House $house
     * @return array
     */
    public function query(House $house): iterable
    {
        return [
            'house' => $house,
            'image' => $house->information->project_image,
            'table' => [
                'status' => '',
                'row_id' => 'id',
                'coords' => 'Координаты',
                'number' => 'Номер этажа',
                'apartments_in_the_floor' => 'Количество квартир',
                'image_floor' => 'План подъезда',
                'settings' => 'Действие',
            ],
            'data' => $house->floors->sortBy('number')
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Разметка этажей';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Назад к проекту'))
                ->icon('arrow-right-circle')
                ->route('platform.houses.edit', ['house' => $this->house->id])
                ->canSee($this->house->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::view('admin.editor.floors-image-markup', ['image' => $this->image, 'table' => $this->table, 'data' => $this->data]),
        ];
    }

    /**
     * @param House $house
     * @param SaveFloorImageMarkupRequest $request
     * @return JsonResponse
     */
    public function saveLine(House $house, SaveFloorImageMarkupRequest $request): JsonResponse
    {
        try {
            /** @var Floor $existsFloor */
            $existsFloor = Floor::query()->where('row_id', $request->input('data.row_id'))
                ->where('house_id', $house->id)->first();

            $sumApartments = $house->floors->sum('apartments_in_the_floor');

            if($existsFloor){
                $sumApartments = $sumApartments - $existsFloor->apartments_in_the_floor + $request->input('data.apartments_in_the_floor');
            } else {
                $sumApartments = $sumApartments + $request->input('data.apartments_in_the_floor');
            }

            if($house->information->number_of_floors <= $request->input('data.number')) {
                throw new Exception('floors_quota_flats_number');
            }

            if($house->floors->where('number', $request->input('data.number'))->count() > 0) {
                throw new Exception('floors_quota_flats_exists');
            }

            if($house->floors->count() == $house->information->number_of_floors && !$existsFloor){
                throw new Exception('floors_quota_floor');
            }

            if($house->information->number_of_apartments < $sumApartments) {
                throw new Exception('floors_quota_flats_count');
            }

            return response()->json([
                'success' => true,
                'data' => (new HouseImageMarkupService())->updateOrCreate(Auth::user()->id, $request->validated(), $house)
            ],ResponseAlias::HTTP_OK);
        }catch (Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_BAD_REQUEST);
        }
    }

    public function removeLine(House $house, RemoveImageMarkupRequest $request): JsonResponse
    {
        try {
            $floor = Floor::query()->where('row_id', $request->validated()['data']['row_id'])->where('house_id', $house->id)->first();

            if(!$floor){
                return response()->json([
                    'success' => true,
                ],ResponseAlias::HTTP_OK);
            }

            if(!(new HouseImageMarkupService())->delete($floor)){
                throw new RuntimeException('undefined error');
            }

            return response()->json([
                'success' => true,
            ],ResponseAlias::HTTP_OK);
        } catch (RuntimeException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
