<?php

namespace App\Orchid\Screens\Houses\Floors;

use App\Http\Requests\SaveFloorRequest;
use App\Models\Floor;
use App\Models\House;
use App\Orchid\Layouts\Hoses\Flats\FlatsListLayout;
use App\Orchid\Layouts\Hoses\Floors\FloorEditLayout;
use App\Services\FloorService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FloorEditScreen extends Screen
{

    public $floor;

    /**
     * Query data.
     *
     * @param Floor $floor
     * @return array
     */
    public function query(Floor $floor): iterable
    {
        return [
            'floor' => $floor,
            'flats' => $floor->flats
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактирование этажа';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Разметка квавртир'))
                ->icon('crop')
                ->route('platform.house.floor.flats-image-markup', ['house' => $this->floor->house_id, 'floor' => $this->floor->id]),

            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->floor->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block(FloorEditLayout::class)
                ->title(__('Основное описание')),

            FlatsListLayout::class
        ];
    }

    /**
     * @param Floor $floor
     * @param SaveFloorRequest $request
     * @return RedirectResponse
     */
    public function save(Floor $floor, SaveFloorRequest $request): RedirectResponse
    {
        try {
            if($floor->house_id) {
                if(Floor::query()->where('house_id', $floor->house_id)->where('number', $request->input('number'))->count() > 0){
                    throw new \Exception('В доме уже указан выбранный этаж');
                }
            }

            $floorData = (new FloorService())->updateOrCreate(Auth::user()->id, $request->validated(), $floor);

            Toast::info('Этаж успешно сохранён');

            return redirect()->route('platform.house.floor.flats-image-markup', ['house' => $floorData->house_id, 'floor' => $floorData->id]);

        } catch (\Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.houses.list');
    }

    public function remove(Floor $floor): RedirectResponse
    {
        try {
            (new FloorService())->delete($floor);

            Toast::info('Этаж успешно удалён');

        } catch (\Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }


        return redirect()->route('platform.houses.floors.list');
    }

}
