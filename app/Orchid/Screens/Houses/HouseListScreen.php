<?php

namespace App\Orchid\Screens\Houses;

use App\Models\House;
use App\Orchid\Layouts\Hoses\HouseListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class HouseListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'houses' => House::filters()->defaultSort('id', 'desc')->paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список проектов';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Добавить')
                ->icon('plus')
                ->href(route('platform.houses.create')),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            HouseListLayout::class
        ];
    }
}
