<?php

namespace App\Orchid\Screens\Houses;

use App\Http\Requests\SaveHouseRequest;
use App\Models\House;
use App\Orchid\Layouts\Hoses\Flats\FlatsListLayout;
use App\Orchid\Layouts\Hoses\Floors\FloorsListLayout;
use App\Orchid\Layouts\Hoses\HouseEditLayout;
use App\Orchid\Layouts\Hoses\HouseImagesEditLayout;
use App\Orchid\Listeners\HoseTypeListener;
use App\Services\HouseService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Map;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class HouseEditScreen extends Screen
{

    public $house;

    /**
     * Query data.
     *
     * @param House $house
     * @return array
     */
    public function query(House $house): iterable
    {
        return [
            'house' => $house,
            'floors' => $house->floors,
            'flats' => $house->flats()->paginate(20),
            'house.place' => [
                'lat' => $house->exists ? $house->lat : config('constants.geo.map.n-tagil.latitude'),
                'lng' => $house->exists ? $house->lng: config('constants.geo.map.n-tagil.longitude'),
            ],
            'information' => $house->information
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->house->exists) ? 'Редактирование проекта' : 'Добавление проекта';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Разметка этажей'))
                ->icon('crop')
                ->route('platform.houses.floors-image-markup', ['house' => $this->house->id ?? 0])
                ->canSee($this->house->exists && $this->house->information->number_of_floors > $this->house->floors->count()),

            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->confirm('Вы уверены что собираетесь удалить данный объект строительства ?')
                ->canSee($this->house->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block(HouseEditLayout::class)
                ->title(__('Основное описание'))
                ->description(__('Эти данные необходимы для описания проекта')),

            HoseTypeListener::class,

            Layout::block(HouseImagesEditLayout::class)
                ->title(__('Изображения'))
                ->description(__('Изображение для разметки необходимы для интеракивной навигации, остальные будут
                добавлены в гелерю')),

            Layout::rows([
                Map::make('house.place')
                    ->title('Точка на карте')
                    ->help('Укажите координаты или введите название ')
            ])->title('Выбрать место'),

            Layout::rows([ ])->title('Этажи'),
            FloorsListLayout::class,

            Layout::rows([ ])->title('Квартиры'),
            FlatsListLayout::class
        ];
    }

    /**
     * Вовращает поля для заполнения
     * @param array $type
     * @return array|string[]
     */
    public function asyncTypeProjectFields(array $type = ['type_id' => 'ApartmentHouse']): array
    {
        return [
            'type' => $type['type_id']
        ];
    }

    public function save(House $house, SaveHouseRequest $request): RedirectResponse
    {
        try{
            $house = (new HouseService())->updateOrCreate(Auth::user()->id, $request->validated(), $house);

            if($house->status_create_info  == 'create_project'){
                return redirect()->route('platform.houses.floors-image-markup', ['house' => $house->id]);
            }

        } catch (\Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.houses.list');
    }

    public function remove(House $house): RedirectResponse
    {
        DB::transaction(function () use ($house){
            foreach ($house->getRelations() as $relation){
                $house->{$relation}()->delete();

                $house->delete();
            }
        });

        Toast::info('Объект строительства успешно удалён');

        return redirect()->route('platform.houses');
    }

}
