<?php

namespace App\Orchid\Screens\Houses\Flats;

use App\Models\Flat;
use App\Orchid\Layouts\Hoses\Flats\FlatsListLayout;
use App\Orchid\Layouts\Hoses\HouseListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class HouseFlatsListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'flats' => Flat::filters()->defaultSort('id', 'desc')->paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список квартир';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            FlatsListLayout::class
        ];
    }
}
