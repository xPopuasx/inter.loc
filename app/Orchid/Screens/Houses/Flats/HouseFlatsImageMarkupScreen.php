<?php

namespace App\Orchid\Screens\Houses\Flats;

use App\Http\Requests\RemoveImageMarkupRequest;
use App\Http\Requests\SaveFlatImageMarkupRequest;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use App\Services\HouseFlatsImageMarkupService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class HouseFlatsImageMarkupScreen extends Screen
{

    public $house;
    public $image;
    public $table;
    public $data;
    public $floor;

    /**
     * @param House $house
     * @param Floor $floor
     * @return iterable
     */
    public function query(House $house, Floor $floor): iterable
    {
        return [
            'house' => $house,
            'floor' => $floor,
            'image' => $floor->image_floor,
            'table' => [
                'status' => '',
                'row_id' => 'id',
                'square_meters' => 'S M <sup>2</sup>',
                'live_area_square_meters' => 'S жилая M <sup>2</sup>',
                'square_meters_kitchen' => 'S Кухня M <sup>2</sup>',
                'square_meters_bathroom' => 'S Ванная к. M <sup>2</sup>',
                'number_of_rooms' => 'Количество комнат',
                'image_flat' => 'План квартиры',
                'settings' => 'Действие'
            ],
            'data' => $floor->flats->sortBy('row_id')
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Разметка квартир';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Перейти к этажу')
                ->route('platform.floor.edit', $this->floor->id)
                ->icon('action-redo')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::view('admin.editor.flats-image-markup', ['image' => $this->image, 'table' => $this->table, 'data' => $this->data]),
        ];
    }

    /**
     * @param House $house
     * @param Floor $floor
     * @param SaveFlatImageMarkupRequest $request
     * @return JsonResponse
     */
    public function saveLine(House $house, Floor $floor, SaveFlatImageMarkupRequest $request): JsonResponse
    {
        try {
            $existsFlat = Flat::query()->where('row_id', $request->input('data.row_id'))
                ->where('house_id', $house->id)->where('floor_id', $floor->id)->exists();

            if ($floor->flats->count() == $floor->apartments_in_the_floor && !$existsFlat) {
                throw new Exception('flats_quota');
            }

            return response()->json([
                'success' => true,
                'data' => (new HouseFlatsImageMarkupService())->updateOrCreate(Auth::user()->id, $request->validated(), $floor)
            ],ResponseAlias::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param House $house
     * @param Floor $floor
     * @param RemoveImageMarkupRequest $request
     * @return JsonResponse
     */
    public function removeLine(House $house, Floor $floor, RemoveImageMarkupRequest $request): JsonResponse
    {
        try {
            $flat = Flat::query()->where('row_id', $request->validated()['data']['row_id'])->where('floor_id', $floor->id)->first();

            if(!$flat){
                return response()->json([
                    'success' => true,
                ],ResponseAlias::HTTP_OK);
            }

            if(!(new HouseFlatsImageMarkupService())->delete($flat)){
                throw new RuntimeException('undefined error');
            }

            return response()->json([
                'success' => true,
            ],ResponseAlias::HTTP_OK);
        } catch (RuntimeException $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
