<?php

namespace App\Orchid\Screens\Houses\Flats;

use App\Http\Requests\SaveFlatRequest;
use App\Models\Flat;
use App\Models\House;
use App\Orchid\Layouts\Hoses\Flats\FlatEditLayout;
use App\Services\FlatService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FlatEditScreen extends Screen
{

    public $flat;

    /**
     * Query data.
     *
     * @param Flat $flat
     * @return array
     */
    public function query(Flat $flat): iterable
    {
        return [
            'flat' => $flat,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактирование кваритры';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Перейти к проекту')
                ->route('platform.houses.edit', $this->flat->house_id)
                ->icon('action-redo'),
            Link::make('Перейти к этажу')
                ->route('platform.floor.edit', $this->flat->floor_id)
                ->icon('action-redo'),

            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->flat->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(FlatEditLayout::class)
                ->title(__('Основное описание'))
        ];
    }

    /**
     * @param Flat $flat
     * @param SaveFlatRequest $request
     * @return RedirectResponse
     */
    public function save(Flat $flat, SaveFlatRequest $request): RedirectResponse
    {
        try {
            $flatData = (new FlatService())->updateOrCreate(Auth::user()->id, $request->validated(), $flat);

            Toast::info('Кватира успешно сохранена');

            return redirect()->route('platform.floor.edit', $flatData->floor_id);

        } catch (\Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.houses.list');
    }

    public function remove(Flat $flat): RedirectResponse
    {
        try {
            (new FlatService())->delete($flat);

            Toast::info('Квартира успешно удалена');

        } catch (\Exception $e){
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.houses.flats.list');
    }

}
