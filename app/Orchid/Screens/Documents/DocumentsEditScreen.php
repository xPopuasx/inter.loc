<?php

namespace App\Orchid\Screens\Documents;

use App\Http\Requests\SaveDocumentRequest;
use App\Http\Requests\SavePostRequest;
use App\Models\Document;
use App\Models\Tag;
use App\Orchid\Layouts\Documents\DocumentsEditLayout;
use App\Services\AttachmentService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class DocumentsEditScreen extends Screen
{
    public $document;

    public $tags;
    /**
     * Query data.
     *
     * @return array
     */
    public function query(Document $document): iterable
    {
        return [
            'document' => $document->load('attachments'),
            'tags' => Tag::query()->where('type', Tag::DOCUMENTS)->get()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->document->exists) ? 'Редактирование документа' : 'Добавление документа';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->document->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block([
                DocumentsEditLayout::class,
            ])
            ->title('Документы')
            ->description('Документы необходимы для загрузки документов на сайты, что бы пользовтаели могли их скачать')];
    }

    public function save(SaveDocumentRequest $request, Document $document) : RedirectResponse
    {
        $user = Auth::user();

        $attachments = $request->input('document.attachments') ?? [];

        try{
            $data = $request->input('document');
            $data['user_id'] = $user->id;
            $data['status_id'] = 2;

            foreach ($attachments as $attachment){
                $data['attachment_id'] = $attachment;

                $document->exists
                    ? $document->update($data)
                    : Document::create($data);
            }

            Toast::info($document->exists ? 'Документ успешно изменён' : 'Документ успешно сохранён');

        } catch (Exception $e){
            Attachment::query()->whereIn('id', $attachments)->delete();
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.documents.list');
    }

    public function remove(Document $document): RedirectResponse
    {
        try{
            DB::beginTransaction();

            $attachment = Attachment::find($document->attachment_id);

            if(AttachmentService::deleteStorageFile($attachment)){
                $document->delete();

                $attachment->delete();
            } else {
                DB::rollBack();
                throw new Exception('файл не найден в директории');
            }

            Toast::info(__('Документ успешно удалён'));

            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
            Toast::info('Чтото пошло не так ' . $e->getMessage());
        }

        return redirect()->route('platform.documents.list');
    }
}
