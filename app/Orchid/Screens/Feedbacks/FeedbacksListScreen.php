<?php

namespace App\Orchid\Screens\Feedbacks;

use App\Http\Requests\SetStatusFeedbackRequest;
use App\Models\Feedback;
use App\Orchid\Layouts\Feedbacks\FeedbacksListLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Orchid\Screen\Action;
use Orchid\Screen\Screen;

class FeedbacksListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'feedbacks' => Feedback::filters()->defaultSort('id', 'desc')->paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Список запросов';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            FeedbacksListLayout::class
        ];
    }

    public function setStatus(SetStatusFeedbackRequest $request, Feedback $feedback): RedirectResponse
    {
        $user = Auth::user();

        Log::info('Пользовтель c id '. $user->id . ' сменил статус на' . $request->input('status'));

        Feedback::query()->where('id', $feedback->id)->update($request->validated());

        return redirect()->route('platform.feedbacks.list');
    }
}
