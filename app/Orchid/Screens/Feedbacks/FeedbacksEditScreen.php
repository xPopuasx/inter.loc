<?php

namespace App\Orchid\Screens\Feedbacks;

use App\Models\Feedback;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class FeedbacksEditScreen extends Screen
{
    public $feedback;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Feedback $feedback): iterable
    {
        return [
            'feedback' => $feedback,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return ($this->feedback->exists) ? 'Просмотр обращения' : '';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            DropDown::make('Сменить статус')
                ->icon('folder-alt')
                ->list([

                    Button::make('Action')
                        ->method('setStatus')
                        ->icon('bag'),

                    Button::make('Another action')
                        ->method('setStatus')
                        ->icon('bubbles'),

                    Button::make('Something else here')
                        ->method('setStatus')
                        ->icon('bulb'),
                ]),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('feedback', [
                Sight::make('id')->popover('Идентификатор заявки в системе'),
                Sight::make('type', 'Тип заявки')->render(function (Feedback $feedback) {
                    return Feedback::TYPES[$feedback->type];
                }),
                Sight::make('full_name', 'Имя указанное в заявке'),
                Sight::make('phone', 'Телефон указанный в заявке'),
                Sight::make('email', 'Почта указанная в заявке'),
                Sight::make('status', 'Статус заявки')->render(function (Feedback $feedback) {
                    return $feedback->status == 'create'
                        ? '<i class="text-danger">●</i> ' . Feedback::STATUSES[$feedback->status]
                        : '<i class="text-success">●</i> ' . Feedback::STATUSES[$feedback->status];
                }),
                Sight::make('description', 'Текст обращения')
            ])->title('Заявка')
        ];
    }

    public function setStatus(Request $request, Feedback $feedback) : RedirectResponse
    {
        return redirect()->route('platform.feedbacks.list');
    }

}
