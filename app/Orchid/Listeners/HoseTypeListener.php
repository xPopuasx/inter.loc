<?php

namespace App\Orchid\Listeners;

use App\Orchid\Layouts\Hoses\HosesDescriptions\ApartmentHouseDescriptionEditLayout;
use App\Orchid\Layouts\Hoses\HosesDescriptions\CottageDescriptionEditLayout;
use App\Orchid\Layouts\Hoses\HouseTypeLayout;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Listener;
use Orchid\Support\Facades\Layout;
use ReflectionClass;

class HoseTypeListener extends Listener
{
    /**
     * List of field names for which values will be listened.
     *
     * @var string[]
     */
    protected $targets = [
        'project.type_id'
    ];

    /**
     * What screen method should be called
     * as a source for an asynchronous request.
     *
     * The name of the method must
     * begin with the prefix "async"
     *
     * @var string
     */
    protected $asyncMethod = 'asyncTypeProjectFields';

    /**
     * @return Layout[]
     * @throws \ReflectionException
     */
    protected function layouts(): iterable
    {
        $className = $this->query->get('type') ?? "ApartmentHouse";

        return [
            Layout::block(HouseTypeLayout::class)
                ->title(__('Тип'))
                ->description(__('В зависимости от типа будут заполняться свойства')),
            Layout::block($this->getClass($className))
                ->title(__('Основное описание'))
                ->description(__('Эти данные необходимы для описания проекта'))
        ];
    }

    /**
     * @param string $className
     * @return string
     */
    private function getClass(string $className): ?string
    {
        if ($className == 'ApartmentHouse') {
            return ApartmentHouseDescriptionEditLayout::class;
        } elseif ($className == 'Cottage') {
            return CottageDescriptionEditLayout::class;
        }

        return null;
    }
}
