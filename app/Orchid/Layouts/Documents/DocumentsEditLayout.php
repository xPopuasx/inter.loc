<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Documents;

use App\Models\Tag;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class DocumentsEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('document.file_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название документа'))
                ->placeholder('Укажите название документа')
                ->help(__('По данному полю вы будите ориентироваться в поиске документа')),

            Select::make('document.tag_id')
                ->options(Tag::getTagByType(Tag::DOCUMENTS) ?? [])
                ->required()
                ->title(__('Выбрать тэг'))
                ->help(__('Данное поле ограничит выборку в документах')),

            Upload::make('document.attachments')
                ->maxFiles(1)
                ->value(true)
                ->groups(Tag::DOCUMENTS),
        ];
    }
}
