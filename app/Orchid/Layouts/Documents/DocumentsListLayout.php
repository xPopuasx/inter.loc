<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Documents;

use App\Models\Document;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class DocumentsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'documents';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Document $document) {
                    return $document->id;
                }),
            TD::make('file_name', 'Название')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Document $document) {
                    return Link::make($document->file_name)
                        ->route('platform.documents.edit', $document->id);
                }),

            TD::make('title', 'Тэг')
                ->cantHide()
                ->render(function (Document $document) {
                    return Link::make($document->tag->title)
                        ->route('platform.tags.edit', $document->tag->id);
                }),

            TD::make('Вложения', 'Вложения')
                ->cantHide()
                ->render(function (Document $document) {
                    foreach ($document->attachments as $attachment){
                        return mb_strimwidth($attachment->original_name, 0, 55, '...');
                    };
                }),

            TD::make('updated_at', __('Дата обновления'))
                ->sort()
                ->render(function (Document $document) {
                    return $document->updated_at->toDateTimeString();
                }),
        ];
    }
}
