<?php

namespace App\Orchid\Layouts\Contents\House;

use App\Models\House;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class HouseContent extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param House $house
     *
     * @return View
     */
    public function render(House $house): View
    {
        return view($this->template, [
            'title'    => $house->title,
            'subTitle' => $house->address,
            'url' => route('platform.houses.edit', $house->id),
            'image' => ''
        ]);
    }
}
