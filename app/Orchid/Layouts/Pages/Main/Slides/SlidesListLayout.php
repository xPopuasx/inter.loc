<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Pages\Main\Slides;

use App\Models\Pages\Main\Slide;
use App\Models\Post;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class SlidesListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'slides';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->sort()
                ->filter(Input::make())
                ->render(function (Slide $slide){
                    return $slide->id;
                }),

            TD::make('title', 'Название')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Slide $slide) {
                    return Link::make($slide->title)
                        ->route('platform.pages.main.slides.edit', $slide->id);
                }),
        ];
    }
}
