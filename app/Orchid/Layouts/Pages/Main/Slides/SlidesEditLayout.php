<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Pages\Main\Slides;

use App\Models\House;
use App\Models\Tag;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class SlidesEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('slide.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название слайда'))
                ->placeholder('Укажите название слайда'),

            TextArea::make('slide.description')
                ->max(255)
                ->required()
                ->title(__('Описание слайда'))
                ->placeholder('Укажите описание слайда'),

            Cropper::make('slide.slide_image')
                ->title('Изображение для слайда планировок')
        ];
    }
}
