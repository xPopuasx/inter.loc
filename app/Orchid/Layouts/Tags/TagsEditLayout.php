<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Tags;

use App\Models\Tag;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class TagsEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('tag.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название тэга'))
                ->placeholder('Укажите название тэга')
                ->help(__('По данному полю вы будите ориентироваться в поиске тэга')),

            Select::make('tag.type')
                ->options(Tag::TYPES)
                ->required()
                ->title(__('Тип тэга'))
                ->help(__('Данное поле ограничит выборку в разделах тэга')),
        ];
    }
}
