<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Tags;

use App\Models\Tag;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TagsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'tags';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('title', 'Название')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Tag $tag) {
                    return Link::make($tag->title)
                        ->route('platform.tags.edit', $tag->id);
                }),

            TD::make('type', __('Тип'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Tag $tag) {
                    return Tag::TYPES[$tag->type];
                }),

            TD::make('user', __('Добавил'))
                ->render(function (Tag $tag) {
                    return $tag->user->email;
                }),

            TD::make('created_at', __('Дата обновления'))
                ->sort()
                ->render(function (Tag $tag) {
                    return $tag->updated_at->toDateTimeString();
                }),
        ];
    }
}
