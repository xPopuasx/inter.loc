<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Feedbacks;

use App\Models\Feedback;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FeedbacksListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'feedbacks';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Feedback $feedback) {
                    return $feedback->id;
                }),

            TD::make('file_name', 'Отправитель')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Feedback $feedback) {
                    return Link::make($feedback->full_name)
                        ->route('platform.feedbacks.show', $feedback->id);
                }),

            TD::make('phone', 'телефон')
                ->sort()
                ->filter(Input::make())
                ->render(function (Feedback $feedback) {
                    return $feedback->phone;
                }),

            TD::make('email', 'Почта')
                ->sort()
                ->filter(Input::make())
                ->render(function (Feedback $feedback) {
                    return $feedback->email;
                }),

            TD::make('type', 'Тип заявки')
                ->cantHide()
                ->sort()
                ->render(function (Feedback $feedback) {
                    return Feedback::TYPES[$feedback->type];
                }),

            TD::make('status', 'Статус')
                ->cantHide()
                ->sort()
                ->render(function (Feedback $feedback) {
                    return Feedback::STATUSES[$feedback->status];
                }),

            TD::make('updated_at', __('Дата обновления'))
                ->cantHide()
                ->sort()
                ->render(function (Feedback $feedback) {
                    return $feedback->updated_at->toDateTimeString();
                }),

            TD::make(__('Действия'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Feedback $feedback) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Просмотр'))
                                ->icon('eye')
                                ->route('platform.feedbacks.show', ['feedback' => $feedback->id]),

                            Button::make(__('Сменить статус'))
                                ->icon('number-list')
                                ->confirm(__('Выбрать статус'.
                                    Select::make('status')
                                        ->options(Feedback::STATUSES)
                                ))
                                ->method('setStatus', [
                                    'id' => $feedback->id,
                                    'status' => 2,
                                ]),
                        ]);

                }),
        ];
    }
}
