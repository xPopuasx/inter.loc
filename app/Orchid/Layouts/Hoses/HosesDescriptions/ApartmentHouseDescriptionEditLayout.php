<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\HosesDescriptions;

use App\Models\House;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class ApartmentHouseDescriptionEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('information.total_square_meters')
                ->type('number')
                ->title('Квадратные метры')
                ->required()
                ->placeholder('Укажитео общую площадь объекта'),

            Input::make('information.number_of_floors')
                ->type('number')
                ->title('Количество этажей в доме')
                ->required()
                ->placeholder('Укажите количество этажей в доме'),

            Input::make('information.number_of_apartments')
                ->type('number')
                ->title('Количество квартир в доме')
                ->required()
                ->placeholder('Укажите количество квартир в доме'),

            Input::make('information.number_of_entrances')
                ->type('number')
                ->title('Количество подъездов в доме')
                ->required()
                ->placeholder('Укажите количество подъездов в доме'),

            Input::make('information.parking_places')
                ->type('number')
                ->title('Количество парковочных мест')
                ->required()
                ->placeholder('Укажите количество парковочных мест'),

            CheckBox::make('information.has_underground_parking')
                ->title('Имеется ли подземный паркинг'),

            CheckBox::make('information.has_playground')
                ->title('Имеется ли деская площадка'),

            CheckBox::make('information.has_fenced_area')
                ->title('Имеется ли огороженная территория'),

            CheckBox::make('information.has_arrangement_of_the_territory')
                ->title('Имеется ли благоустройстов территории'),

            CheckBox::make('information.has_concierge_system')
                ->title('Имеется ли консьерж система'),


            Upload::make('house.attachment')
                ->groups(House::PREVIEW_IMAGES['presentation'])
                ->title('Презентация проекта'),

            Cropper::make('information.project_image')
                ->groups(House::PREVIEW_IMAGES['main_images'])
                ->title('Изображение для разметки планировок'),

            Cropper::make('information.header_image')
                ->groups(House::PREVIEW_IMAGES['header_image'])
                ->title('Изображение вставки в заголовок'),
        ];
    }
}
