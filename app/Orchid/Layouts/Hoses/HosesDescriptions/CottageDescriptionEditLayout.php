<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\HosesDescriptions;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class CottageDescriptionEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('house.number_of_apartments')
                ->type('number')
                ->title('Количество этажей в доме')
                ->required()
                ->placeholder('Укажите количество этажей'),

            Input::make('house.number_of_floors')
                ->type('number')
                ->title('Количество квартир в доме')
                ->required()
                ->placeholder('Укажите количество квартир'),

        ];
    }
}
