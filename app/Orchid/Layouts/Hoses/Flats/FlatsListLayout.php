<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\Flats;

use App\Models\Flat;
use App\Models\Floor;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FlatsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'flats';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->sort()
                ->filter(Input::make())
                ->render(function (Flat $flat) {
                    return $flat->id;
                }),

            TD::make('x', 'Координата x')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Flat $flat) {
                    return $flat->x;
                }),

            TD::make('y', 'Координата y')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Flat $flat) {
                    return $flat->y;
                }),

            TD::make('project_name', 'Проект')
                ->render(function (Flat $flat){
                    return $flat->house->title;
                }),

            TD::make('project_name', 'Этаж')
                ->render(function (Flat $flat){
                    return $flat->floor->number;
                }),

            TD::make('project_name', 'S м')
                ->render(function (Flat $flat){
                    return $flat->square_meters;
                }),

            TD::make('description', 'Описание')
                ->render(function (Flat $flat){
                    return isset($flat->description) ? 'Есть': 'Отсутствует';
                }),

            TD::make('number_of_rooms', 'Количество комнат')
                ->render(function (Flat $flat){
                    return $flat->number_of_rooms;
                }),

            TD::make('status', 'Статус')
                ->render(function (Flat $flat){
                    return $flat->statusName;
                })
            ->sort(),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Flat $flat) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make('Перейти к проекту')
                                ->route('platform.houses.edit', $flat->house_id)
                                ->icon('action-redo'),
                            Link::make('Перейти к этажу')
                                ->route('platform.floor.edit', $flat->floor_id)
                                ->icon('action-redo'),
                            Link::make('Редактировать')
                                ->route('platform.flat.edit', $flat->id)
                                ->icon('pencil')
                        ]);
                }),

        ];
    }
}
