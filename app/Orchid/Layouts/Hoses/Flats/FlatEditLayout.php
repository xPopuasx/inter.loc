<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\Flats;

use App\Models\House;
use App\Models\Tag;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class FlatEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('flat.row_id')
                ->type('text')
                ->title('flat X')
                ->disabled(),

            Input::make('flat.x')
                ->type('text')
                ->title('Ось X')
                ->disabled(),

            Input::make('flat.y')
                ->type('text')
                ->title('Ось Y')
                ->disabled(),

            Input::make('flat.excursion_url')
                ->type('text')
                ->title('Ссылка на 3D экскурсию'),

            Select::make('flat.status')
                ->title('Статус')
                ->options(['1' => 'В продаже', '2' => 'Продано', '3' => 'Бронь', '4' => 'Скрыто']),

            Input::make('flat.price')
                ->type('number')
                ->title('Цена')
                ->placeholder('Укажите цену'),

            Input::make('flat.number')
                ->type('number')
                ->title('Номер')
                ->required()
                ->placeholder('Укажите номер'),

            Input::make('flat.discount')
                ->type('number')
                ->title('Скидка')
                ->placeholder('Укажите скидку'),

            Input::make('flat.square_meters')
                ->type('number')
                ->required()
                ->title('Площадь')
                ->placeholder('Укажите площадь'),

            Input::make('flat.live_area_square_meters')
                ->title('Жилая площадь')
                ->required()
                ->placeholder('Укажите жилую площадь'),

            Input::make('flat.square_meters_bathroom')
                ->title('Площадь ванной комнаты')
                ->placeholder('Укажите площадь ванной комнаты'),

            Input::make('flat.square_meters_kitchen')
                ->type('number')
                ->title('Площадь кухни')
                ->placeholder('Укажите площадь кухни'),

            Input::make('flat.number_of_rooms')
                ->type('number')
                ->title('Количество комнат')
                ->required()
                ->placeholder('Укажите количество комнат'),


            Input::make('flat.ceiling_height')
                ->type('float')
                ->title('Высота потолков')
                ->placeholder('Укажите высоту потолков'),

            TextArea::make('flat.description')
                ->title('Дополнительное описание')
                ->placeholder('Укажите дополнительное описание'),

            Select::make('flat.tags')
                ->title('Теги')
                ->fromQuery(Tag::query()->where('type', Tag::FLATS), 'title')
                ->multiple(),

            Switcher::make('flat.has_balcony')
                ->sendTrueOrFalse()
                ->placeholder('Есть блкон'),

            Switcher::make('flat.has_loggia')
                ->sendTrueOrFalse()
                ->placeholder('Есть лоджия'),

            Switcher::make('flat.has_free_layout')
                ->sendTrueOrFalse()
                ->value(0)
                ->placeholder('Есть вободная планировка'),

            Switcher::make('flat.has_separate_bathroom')
                ->sendTrueOrFalse()
                ->value(0)
                ->placeholder('Раздельный санузел'),

            Cropper::make('flat.image_flat')
                ->groups(House::PREVIEW_IMAGES['image_flat'])
                ->title('Планировка квартиры'),

            Upload::make('flat.attachment')
                ->groups(House::PREVIEW_IMAGES['image_flat_for_gallery'])
                ->title('Изображения для галереи (максимум 5)')
                ->maxFiles(5)
        ];
    }
}
