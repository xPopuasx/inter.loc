<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\Floors;

use App\Models\Floor;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FloorsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'floors';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->id;
                }),

            TD::make('x', 'Координата x')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->x;
                }),

            TD::make('y', 'Координата y')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->y;
                }),

            TD::make('number', 'Этаж')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->number;
                }),

            TD::make('apartments_in_the_floor', 'Количество квартир на этаже')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->apartments_in_the_floor;
                }),

            TD::make('apartments_in_the_floor', 'Размечено квартир')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->flats->count();
                }),

            TD::make('house_id', 'Проект')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Floor $floor) {
                    return $floor->house->title;
                }),

            TD::make('%', '%')
                ->cantHide()
                ->render(function (Floor $floor) {
                    return $floor->apartments_in_the_floor > 0 ?
                        round(($floor->flats->count() / $floor->apartments_in_the_floor) * 100, 2). ' %'
                        : '100 %';
                }),


            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Floor $floor) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make('Перейти к проекту')
                                ->route('platform.houses.edit', $floor->house_id)
                                ->icon('action-redo'),
                            Link::make('Редактировать')
                                ->route('platform.floor.edit',$floor->id)
                                ->icon('pencil'),
                            Link::make('Разметка квартир')
                                ->route('platform.house.floor.flats-image-markup', ['house' => $floor->house_id, 'floor' => $floor->id])
                                ->icon('crop')
                        ]);
                }),

        ];
    }
}
