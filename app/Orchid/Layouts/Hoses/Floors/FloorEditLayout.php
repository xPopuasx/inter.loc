<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses\Floors;

use App\Models\House;
use Faker\Provider\Text;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class FloorEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('floor.row_id')
                ->type('text')
                ->required()
                ->title('Ось X')
                ->disabled(),

            Input::make('floor.x')
                ->type('text')
                ->required()
                ->title('Ось X')
                ->disabled(),

            Input::make('floor.y')
                ->type('text')
                ->required()
                ->title('Ось Y')
                ->disabled(),

            Input::make('floor.number')
                ->type('number')
                ->required()
                ->title('Номер этажа')
                ->placeholder('Укажите номер этажа'),

            Input::make('floor.apartments_in_the_floor')
                ->type('number')
                ->title('Количество квартир')
                ->required()
                ->placeholder('Укажите количество квартир'),

            TextArea::make('floor.description')
                ->title('Дополнительное описание')
                ->placeholder('Укажите дополнительное описание'),

            Cropper::make('floor.image_floor')
                ->groups(House::PREVIEW_IMAGES['image_floor'])
                ->title('Изображение для разметки планировок'),

            Upload::make('floor.attachment')
                ->groups(House::PREVIEW_IMAGES['image_floor_for_gallery'])
                ->title('Изображения для галереи (максимум 5)')
                ->maxFiles(5),
        ];
    }
}
