<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses;

use App\Models\House;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class HouseImagesEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Upload::make('house.attachment')
                ->groups(House::PREVIEW_IMAGES['images_for_gallery'])
                ->title('Изображения для галереи (максимум 10)')
                ->maxFiles(10)
        ];
    }
}
