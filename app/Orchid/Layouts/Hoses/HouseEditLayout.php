<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class HouseEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('house.title')
                ->type('text')
                ->required()
                ->title('Название проекта')
                ->placeholder('Укажите название'),

            Quill::make('house.description')
                ->title('Краткое описание')
                ->required()
                ->placeholder('Укажите описание'),

            Input::make('house.address')
                ->type('text')
                ->title('Адрес строительства объекта')
                ->required()
                ->placeholder('Укажите адрес'),

            Input::make('house.delivery_year')
                ->type('number')
                ->title('Год сдачи проекта')
                ->required()
                ->placeholder('Укажите год сдачи проекта'),

            Input::make('house.quarter')
                ->type('number')
                ->title('Квартал сдачи проекта')
                ->required()
                ->placeholder('Укажите квартал сдачи проекта'),
        ];
    }
}
