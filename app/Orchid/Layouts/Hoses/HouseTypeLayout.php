<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class HouseTypeLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Select::make('house.type_id')
                ->title('Тип Проекта')
                ->value($this->query->get('type'))
                ->options(config('constants.houses.types_ru'))
        ];
    }
}
