<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Hoses;

use App\Models\House;
use App\Orchid\Layouts\Contents\House\HouseContent;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class HouseListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'houses';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return $house->id;
                }),

            TD::make('title', 'Название')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return new HouseContent($house);
                }),

            TD::make('type_id', 'Тип проекта')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return config('constants.houses.types_ru.'.$house->type_id);
                }),

            TD::make('delivery_year', 'Год сдачи')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return $house->delivery_year;
                }),

            TD::make('quarter', 'Квартал сдачи')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return $house->quarter;
                }),

            TD::make('count_floors', 'Размечено этажей')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return ($house->information)
                        ? $house->floors->count() . ' ('. round(($house->floors->count() / $house->information->number_of_floors * 100), 2)  . ' %)'
                        : 0;
                }),

            TD::make('count_flats', 'Размечено квартир')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (House $house) {
                    return ($house->information)
                        ? $house->flats->count()  . ' ('. round(($house->flats->count() / $house->information->number_of_apartments * 100), 2)  . ' %)'
                        : 0;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (House $house) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make('Редактировать')
                                ->route('platform.houses.edit', $house->id)
                                ->icon('pencil'),
                            Link::make(__('Разметка этажей'))
                                ->icon('crop')
                                ->route('platform.houses.floors-image-markup', ['house' => $house->id ?? 0])
                        ]);
                }),


        ];
    }
}
