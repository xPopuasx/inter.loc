<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Posts;

use App\Models\Tag;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class PostsEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('post.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название поста'))
                ->placeholder('Укажите название поста')
                ->help(__('Это поле поможет людям узнать примерную суть поста')),

            Select::make('post.tags')
                ->title('Теги')
                ->fromQuery(Tag::query()->where('type', Tag::POST), 'title')
                ->multiple(),
        ];
    }
}
