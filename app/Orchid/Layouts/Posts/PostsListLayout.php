<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Posts;

use App\Models\Post;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PostsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'posts';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'id')
                ->sort()
                ->filter(Input::make())
                ->render(function (Post $post) {
                    return $post->id;
                }),

            TD::make('title', 'Название')
                ->cantHide()
                ->sort()
                ->filter(Input::make())
                ->render(function (Post $post) {
                    return Link::make($post->title)
                        ->route('platform.posts.edit', $post->id);
                }),

            TD::make('attachment', __('Колич. вложений'))
                ->cantHide()
                ->render(function (Post $post) {
                    return $post->attachment->count();
                }),

            TD::make('updated_at', __('Дата обновления'))
                ->sort()
                ->render(function (Post $post) {
                    return $post->updated_at->toDateTimeString();
                }),
        ];
    }
}
