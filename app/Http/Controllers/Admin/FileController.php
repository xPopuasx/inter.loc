<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AttachmentService;
use Illuminate\Http\Request;
use Orchid\Attachment\Models\Attachment;

class FileController extends Controller
{
    public function delete(Request $request){
        try{
            /** @var Attachment $attachment */
            $attachment = Attachment::query()->find($request->input('file_id'));

            AttachmentService::deleteStorageFile($attachment);

            $attachment->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $e){
            return response()->json(['success' => false, 'error' => $e->getMessage()]);
        }
    }
}
