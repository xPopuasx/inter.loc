<?php

namespace App\Http\Controllers\Pages\Main;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\HouseResource;
use App\Http\Resources\Pages\Main\Slides\SlideResource;
use App\Models\House;
use App\Models\Pages\Main\Slide;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SlideController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $list = Slide::all();

        return SlideResource::collection($list);
    }


    /**
     * @param Slide $slide
     * @return SlideResource
     */
    public function show(Slide $slide): SlideResource
    {
        return new SlideResource($slide);
    }

}
