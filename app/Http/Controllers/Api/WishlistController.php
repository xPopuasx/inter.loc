<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wishlist\Wishlist;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class WishlistController extends Controller
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function create(Request $request): JsonResponse
    {
        $cookieUser = $this->checkCookie($request);

        /** @var Wishlist $wishlist */
        if (!$wishlist = Wishlist::query()->where('user', $cookieUser)->first()) {
            $wishlist = Wishlist::query()->create(['user' => $cookieUser]);
        }

        return response()->json($wishlist->items)->withCookie('sessionToken', $cookieUser);
    }

    public function setItem(Request $request): JsonResponse
    {
        $cookieUser = $this->checkCookie($request);

        /** @var Wishlist $wishlist */
        if (!$wishlist = Wishlist::query()->where('user', $cookieUser)->first()) {
            $wishlist = Wishlist::query()->create(['user' => $cookieUser]);
        }

        if (!$wishlist->items()->where('flat_id', $request->input('flat_id'))->exists()) {
            $wishlist->items()->create(['flat_id' => $request->input('flat_id')]);
        }

        $wishlist = $wishlist->fresh();

        return response()
            ->json($wishlist->items)
            ->withCookie('sessionToken', $cookieUser);
    }

    public function removeItem(Request $request): JsonResponse
    {
        $cookieUser = $this->checkCookie($request);

        /** @var Wishlist $wishlist */
        if (!$wishlist = Wishlist::query()->where('user', $cookieUser)->first()) {
            $wishlist = Wishlist::query()->create(['user' => $cookieUser]);
        }

        $wishlist->items()->where('flat_id', $request->input('flat_id'))->delete();

        $wishlist = $wishlist->fresh();

        return response()
            ->json($wishlist->items)
            ->withCookie('sessionToken', $cookieUser);
    }

    private function checkCookie(Request $request): string
    {
        if ($request->cookie('sessionToken')) {
            return $request->cookie('sessionToken');
        } else {
            setcookie('sessionToken', sha1(rand(1, 99999999999999)), time() + (3600 * 24 * 365));
            return cookie()->get('sessionToken');
        }
    }

}
