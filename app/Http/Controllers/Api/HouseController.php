<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\HouseResource;
use App\Models\House;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class HouseController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $list = House::all();
        $list->load(['information', 'floors' => function ($q) {
            $q->with(['flats' => function ($q) {
                $q->where('status', '!=',  4);
            }]);
        }]);

        return HouseResource::collection($list);
    }


    /**
     * @param House $house
     * @return HouseResource
     */
    public function show(House $house): HouseResource
    {
        $house->load(['information', 'floors' => function ($q) {
            $q->with(['flats' => function ($q) {
                $q->where('status', '!=',  4);
            }]);
        }]);

        return new HouseResource($house);
    }

}
