<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\FloorResource;
use App\Http\Resources\Api\HouseResource;
use App\Models\Floor;
use App\Models\House;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class HouseFloorController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $list = Floor::all();
        $list->load(['flats' => function ($q) {
            $q->where('status', '!=',  4);
        }]);

        return HouseResource::collection($list);
    }


    /**
     * @param House $house
     * @param Floor $floor
     * @return FloorResource
     */
    public function show(House $house, Floor $floor): FloorResource
    {
        $floor->load('flats');

        return new FloorResource($floor);
    }

}
