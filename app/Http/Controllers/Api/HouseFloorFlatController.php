<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\FlatResource;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class HouseFloorFlatController extends Controller
{
    /**
     * @param House $house
     * @param Floor $floor
     * @return AnonymousResourceCollection
     */
    public function index(House $house, Floor $floor): AnonymousResourceCollection
    {
        $list = Floor::all();
        $list->load(['flats' => function ($q) {
            $q->where('status', '!=',  4);
        }]);

        return FlatResource::collection($list);
    }


    /**
     * @param House $house
     * @param Floor $floor
     * @param Flat $flat
     * @return FlatResource
     */
    public function show(House $house, Floor $floor, Flat $flat): FlatResource
    {
        return new FlatResource($flat);
    }

}
