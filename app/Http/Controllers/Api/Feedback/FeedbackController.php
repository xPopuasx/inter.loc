<?php

namespace App\Http\Controllers\Api\Feedback;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Feedback\CreateFeedbackRequest;
use App\Http\Resources\Api\FlatResource;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use App\Services\Core\IpApiService;
use App\Services\FeedbackService;
use App\Services\FlatService;
use App\Services\Telegram\TelegramApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FeedbackController extends Controller
{
    private FeedbackService $feedbackService;
    private TelegramApiService $telegramApiService;

    private IpApiService $ipApiService;

    public function __construct(FeedbackService $feedbackService, TelegramApiService $telegramApiService, IpApiService $ipApiService)
    {
        $this->feedbackService = $feedbackService;
        $this->telegramApiService = $telegramApiService;
        $this->ipApiService = $ipApiService;
    }

    /**
     * @param CreateFeedbackRequest $request
     * @return JsonResponse
     */
    public function create(CreateFeedbackRequest $request): JsonResponse
    {
        if(!$this->ipApiService->check($request->ip())){
            return response()->json('error', 403);
        };

        $this->feedbackService->create($request->toArray());

        $this->telegramApiService->sendBotMessage(
            [
                'text' => (string)view('telegram.messages.feedback-telegram-message',
                    array_merge(['url' => $request->url()], $request->toArray())),
                'parse_mode' => 'html'
            ]);

        return response()->json('ok', 200);
    }

}
