<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\FlatResource;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use App\Services\FlatService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FlatsController extends Controller
{
    private $flatsService;

    public function __construct(FlatService $flatsService)
    {
        $this->flatsService = $flatsService;
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $list = $this->flatsService->list($request->toArray());

        return FlatResource::collection($list);
    }

    public function filters(): array
    {
        $model = new Flat();

        return [
            'filters' => $model->getFilters(),
            'tags' => $model->getTags(),
        ];
    }

}
