<?php

namespace App\Http\Controllers;

use App\Http\Requests\Telegram\CheckMessageRequest;
use Exception;
use Illuminate\Support\Facades\Log;

class BotController extends Controller
{
    /**
     * @throws Exception
     */
    public function checkMessage(CheckMessageRequest $request): void
    {
        $action = $request->input('message.text') ?? $request->input('callback_query.data');

        if (! $action) {
            Log::info('action not found');
        }

    }
}
