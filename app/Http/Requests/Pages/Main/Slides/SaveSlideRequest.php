<?php

namespace App\Http\Requests\Pages\Main\Slides;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveSlideRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'slide.title' => 'required|string',
            'slide.description' => 'required|string|max:255',
            'slide.slide_image' => 'required|string',
        ];
    }
}
