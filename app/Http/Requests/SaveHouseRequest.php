<?php

namespace App\Http\Requests;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveHouseRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $projectRule = [
            'house.title' => ['required','string', Rule::unique('houses', 'title')->ignore($this->route()->house->id ?? null)],
            'house.description' => 'required|string',
            'house.address' => 'required|string',
            'house.delivery_year' => 'required|date_format:Y',
            'house.quarter' => 'required|integer',
            'house.type_id' => 'required|string',
            'house.place' => 'required|array',
            'house.attachment' => 'nullable|array',
        ];

        $informationRule = [];

        if(request()->input('house.type_id') == config('constants.houses.types')[0]){
            $informationRule = [
                'information.total_square_meters' => 'required|integer',
                'information.number_of_floors' => 'required|integer',
                'information.number_of_entrances' => 'required|integer',
                'information.parking_places' => 'required|integer',
                'information.number_of_apartments' => 'required|integer',
                'information.project_image' => 'required|string',
                'information.header_image' => 'required|string'
            ];
        }

        return array_merge($projectRule, $informationRule);
    }
}
