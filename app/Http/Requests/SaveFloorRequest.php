<?php

namespace App\Http\Requests;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveFloorRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'floor.number' => 'required|integer',
            'floor.apartments_in_the_floor' => 'required|integer',
            'floor.description' => 'nullable|string',
            'floor.image_floor' => 'required|string',
            'floor.attachment' => 'nullable|array'
        ];
    }
}
