<?php

namespace App\Http\Requests;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveFloorImageMarkupRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data' => 'required|array',
            'data.row_id' => 'required|integer',
            'data.coords' => 'required|string',
            'data.number' => 'required|string',
            'data.apartments_in_the_floor' => 'required|integer',
            'data.image_floor' => 'required|string',
        ];
    }

    public function attributes(): array
    {
        return [
            'data.row_id' => 'id',
            'data.coords' => 'Координата Y %',
            'data.number' => 'Номер этажа',
            'data.apartments_in_the_floor' => 'Количество квартир',
            'data.image_floor' => 'План подъезда',
        ];
    }
}
