<?php

namespace App\Http\Requests\Api\Feedback;

use App\Http\Requests\Api\FormRequest;
use App\Traits\AdministratorRequest;

class CreateFeedbackRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|string',
            'phone' => 'required|integer',
            'description' => 'required|string',
            'type' => 'nullable|string',
        ];
    }

    public function attributes(): array
    {
        return [
            'full_name' => 'имя',
            'phone' => 'телефон',
        ];
    }
}
