<?php

namespace App\Http\Requests;

use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SavePostRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'post.title' => ['required', 'string', Rule::unique('posts', 'title')->ignore(Route::getCurrentRoute()->originalParameter('post'))],
            'post.tags' => ['nullable', 'array'],
            'post.attachments' => ['array'],
            'post.score' => ['nullable', 'string', 'url'],
        ];
    }
}
