<?php

namespace App\Http\Requests;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveFlatImageMarkupRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data' => 'required|array',
            'data.row_id' => 'required|integer',
            'data.coords' => 'required|string',
            'data.square_meters' => 'required|numeric',
            'data.live_area_square_meters' => 'required|numeric',
            'data.square_meters_kitchen' => 'nullable|numeric',
            'data.square_meters_bathroom' => 'nullable|numeric',
            'data.number_of_rooms' => 'required|integer',
            'data.image_flat' => 'required|string',
            'data.price' => 'required|string',
        ];
    }

    public function attributes(): array
    {
        return [
            'data.row_id' => 'id',
            'data.x' => 'Ось X',
            'data.y' => 'Ось Y',
            'data.square_meters' => 'Полщадь',
            'data.square_meters_kitchen' => 'Площадь кухни',
            'data.square_meters_bathroom' => 'Площадь ванной комнаты',
            'data.number_of_rooms' => 'Количество комнат',
            'data.image_flat' => 'План квартиры',
        ];
    }
}
