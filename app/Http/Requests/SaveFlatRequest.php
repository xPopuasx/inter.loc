<?php

namespace App\Http\Requests;

use App\Models\Tag;
use App\Traits\AdministratorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class SaveFlatRequest extends FormRequest
{
    use AdministratorRequest;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'flat.square_meters' => 'required|numeric',
            'flat.square_meters_kitchen' => 'required|numeric',
            'flat.square_meters_bathroom' => 'required|numeric',
            'flat.number_of_rooms' => 'required|integer',
            'flat.has_balcony' => 'required|integer',
            'flat.description' => 'nullable|string',
            'flat.has_loggia' => 'required|integer',
            'flat.number' => 'required|integer',
            'flat.price' => 'nullable|integer',
            'flat.discount' => 'nullable|integer',
            'flat.status' => 'required|integer',
            'flat.has_free_layout' => 'required|integer',
            'flat.has_separate_bathroom' => 'required|integer',
            'flat.image_flat' => 'required|string',
            'flat.attachment' => 'nullable|array',
            'flat.tags' => 'nullable|array',
            'flat.ceiling_height' => 'nullable|numeric',
            'flat.live_area_square_meters' => 'nullable|numeric',
            'flat.excursion_url' => 'nullable|string',
        ];
    }
}
