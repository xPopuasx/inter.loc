<?php

namespace App\Http\Resources\Api;

use App\Models\HouseInformation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin HouseInformation
 */
class HouseInformationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total_square_meters' => $this->total_square_meters,
            'has_underground_parking' => $this->has_underground_parking,
            'has_parking' => $this->has_parking,
            'has_playground' => $this->has_playground,
            'has_fenced_area' => $this->has_fenced_area,
            'has_arrangement_of_the_territory' => $this->has_arrangement_of_the_territory,
            'has_concierge_system' => $this->has_concierge_system,
            'number_of_floors' => $this->number_of_floors,
            'parking_places' => $this->parking_places,
            'project_image' => $this->project_image,
            'header_image' => $this->header_image,
            'number_of_apartments' => $this->number_of_apartments,
            'number_of_entrances' => $this->number_of_entrances,
        ];
    }
}
