<?php

namespace App\Http\Resources\Api;

use App\Models\House;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin House
 */
class HouseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'title' => $this->title,
            'description' => $this->description,
            'lat'=> $this->lat,
            'lng'=> $this->lng,
            'quarter'=> $this->quarter,
            'delivery_year' => $this->delivery_year,
            'project_readiness' => $this->project_readiness,
            'attachments' => $this->attachment()->where('group', 'images_for_gallery')->get(),
            'information' => new HouseInformationResource($this->whenLoaded('information')),
            'floors' => FloorResource::collection($this->whenLoaded('floors')),
            'flats_count' => $this->flats->count(),
            'free_flat_count' => $this->freeFlatCount,
            'total_flats_square_meters' => $this->totalFlatsSquareMeters,
            'presentation' => $this->presentation
        ];
    }
}
