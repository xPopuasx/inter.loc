<?php

namespace App\Http\Resources\Api;

use App\Models\Flat;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Post
 */
class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'tag' => $this->tag,
            'slug' => $this->slug,
            'score' => $this->score,
            'title' => $this->title,
            'description' => $this->description,
            'created_at' => $this->created_at->format('d.m.Y'),
            'updated_at' => $this->updated_at->format('d.m.Y'),
            'attachment' => $this->attachment
        ];
    }
}
