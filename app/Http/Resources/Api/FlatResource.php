<?php

namespace App\Http\Resources\Api;

use App\Models\Flat;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Flat
 */
class FlatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'coords' => $this->coords,
            'square_meters' => $this->square_meters,
            'square_meters_kitchen' => $this->square_meters_kitchen,
            'square_meters_bathroom' => $this->square_meters_bathroom,
            'number_of_rooms' => $this->number_of_rooms,
            'has_balcony' => $this->has_balcony,
            'has_loggia' => $this->has_loggia,
            'has_free_layout' => $this->has_free_layout,
            'has_separate_bathroom' => $this->has_separate_bathroom,
            'image_flat' => $this->image_flat,
            'description' => $this->description,
            'project_id' => $this->house_id,
            'floor' => $this->floor,
            'price' => $this->price,
            'discount' => $this->discount,
            'number' => $this->number,
            'tags' => $this->tags,
            'status_name' => $this->statusName,
            'live_area_square_meters' => $this->live_area_square_meters,
            'status' => $this->status,
            'excursion_url' => $this->excursion_url
        ];
    }
}
