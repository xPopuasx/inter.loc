<?php

namespace App\Http\Resources\Api;

use App\Models\Floor;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Floor
 */
class FloorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'coords' => $this->coords,
            'number' => $this->number,
            'apartments_in_the_floor' => $this->apartments_in_the_floor,
            'image_floor' => $this->image_floor,
            'description' => $this->description,
            'project_id' => $this->house_id,
            'flats' => FlatResource::collection($this->whenLoaded('flats'))
        ];
    }
}
