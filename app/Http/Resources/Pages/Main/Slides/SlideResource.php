<?php

namespace App\Http\Resources\Pages\Main\Slides;

use App\Models\Flat;
use App\Models\Pages\Main\Slide;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Slide
 */
class SlideResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'slide_image' => $this->slide_image,
        ];
    }
}
