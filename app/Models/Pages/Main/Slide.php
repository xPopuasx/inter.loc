<?php

namespace App\Models\Pages\Main;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $description
 * @property string $title
 * @property string $slide_image
 */
class Slide extends Model
{
    use AsSource, Filterable, HasFactory;

    protected $fillable = [
        'title',
        'description',
        'slide_image'
    ];
}
