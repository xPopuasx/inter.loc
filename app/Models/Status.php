<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Platform\Models\User;

/**
 * Class Status
 * @package App\Models
 * @property string $title
 * @property string $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|User $user
 */
class Status extends Model
{
    use HasFactory;

    public $fillable =[
        'title',
        'type',
    ];
}
