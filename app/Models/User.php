<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Orchid\Platform\Models\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $description
 * @property string $permissions
 * @property bool $is_blocked
 * @property string $api_token
 * @property bool $agree_to_data_processing
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permissions',
        'api_token',
        'is_blocked',
        'agree_to_data_processing'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'permissions',
        'updated_at',
        'email_verified_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions'          => 'array',
        'email_verified_at'    => 'datetime',
    ];

    /**
     * The attributes for which you can use filters in url.
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'name',
        'email',
        'permissions',
    ];

    /**
     * The attributes for which can use sort in url.
     *
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'name',
        'email',
        'updated_at',
        'created_at',
    ];
}
