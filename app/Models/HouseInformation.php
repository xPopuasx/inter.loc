<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HouseInformation
 * @package App\Models
 * @property int $id
 * @property float $total_square_meters
 * @property bool $has_underground_parking
 * @property bool $has_parking
 * @property bool $has_playground
 * @property bool $has_fenced_area
 * @property bool $has_arrangement_of_the_territory
 * @property bool $has_concierge_system
 * @property int $number_of_floors
 * @property int $parking_places
 * @property string $project_image
 * @property string $header_image
 * @property int $number_of_apartments
 * @property int $number_of_entrances
 * @property int $house_id
 */
class HouseInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_square_meters',
        'has_underground_parking',
        'has_parking',
        'has_playground',
        'has_fenced_area',
        'has_arrangement_of_the_territory',
        'has_concierge_system',
        'number_of_floors',
        'parking_places',
        'project_image',
        'number_of_apartments',
        'number_of_entrances',
        'header_image',
        'house_id'
    ];
}
