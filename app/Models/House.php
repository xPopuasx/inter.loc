<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * Class Project
 * @package App\Models
 * @property int $id
 * @property string $title
 * @property string $address
 * @property string $description
 * @property int $type_id
 * @property float $lat
 * @property float $lng
 * @property int $quarter
 * @property int $project_readiness
 * @property int $attachment
 * @property int $delivery_year
 * @property string $status_create_info
 * @property HouseInformation $information
 * @property Collection $floors
 * @property Collection $flats
 * @property Collection $freeFlatCount
 * @property float $totalFlatsSquareMeters
 * @property Attachment|null $presentation
 */
class House extends Model
{
    use Filterable, AsSource, Chartable, HasFactory, Attachable;


    const PREVIEW_IMAGES = [
        'presentation' => 'presentation',
        'header_image' => 'header_image',
        'main_images' => 'main_images',
        'image_floor' => 'image_floor',
        'image_flat' => 'image_flat',
        'image_floor_for_gallery' => 'image_floor_for_gallery',
        'image_flat_for_gallery' => 'image_flat_for_gallery',
        'images_for_gallery' => 'images_for_gallery'
    ];

    public $fillable = [
        'title',
        'description',
        'address',
        'type_id',
        'lat',
        'lng',
        'quarter',
        'delivery_year',
        'project_readiness',
        'status_create_info'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'title',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'text',
        'type_id',
        'updated_at',
        'created_at',
    ];

    protected $relations = [
        'information',
        'floors',
        'flats'
    ];

    public function getFreeFlatCountAttribute(): int
    {
        return $this->flats()->where('status', 1)->count();
    }

    public function getPresentationAttribute(): ?Attachment
    {
        return $this->attachment()->where('group', House::PREVIEW_IMAGES['presentation'])->first();
    }


    public function getTotalFlatsSquareMetersAttribute(): float
    {
        return $this->flats()->sum('square_meters');
    }


    public function information(): hasOne
    {
        return $this->hasOne(HouseInformation::class);
    }

    public function floors(): HasMany
    {
        return $this->hasMany(Floor::class)->orderBy('number');
    }

    public function flats(): HasMany
    {
        return $this->hasMany(Flat::class);
    }
}
