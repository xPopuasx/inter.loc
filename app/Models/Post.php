<?php

namespace App\Models;

use App\Models\Traits\HasTagsTrait;
use App\Observers\PostObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Attachment\Models\Attachmentable;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Platform\Models\User;
use Orchid\Screen\AsSource;

/**
 * Class Post
 * @package App\Models
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $score
 * @property string $tag_id
 * @property string $user_id
 * @property string $status_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|Tag $tag
 * @property Collection|Status $status
 * @property Collection|User $user
 */
class Post extends Model
{
    use Filterable, AsSource, Chartable, HasFactory, Attachable, HasTagsTrait;

    public $fillable =[
        'title',
        'slug',
        'description',
        'score',
        'tag_id',
        'user_id',
        'status_id',
    ];

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'title',
        'updated_at',
    ];

    protected $with=[
        'attachment'
    ];

    public function status(): HasOne
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function boot()
    {
        parent::boot();

        Post::observe(PostObserver::class);
    }

    public function getTags(): array
    {
        $tags = [];

        $tagIds = Tag::query()->where('type', Tag::POST)->pluck('id')->toArray();

        $existTagIds = collect([]);

        self::query()->with('tags')->whereHas('tags', function ($q) use ($tagIds) {
            $q->whereIn('tag_id', $tagIds);
        })->get()->pluck('tags')->map(function ($tags) {
            return $tags->pluck('id');
        })->each(function ($ids) use (&$existTagIds) {
            $existTagIds = $existTagIds->merge($ids);
        });

        foreach ($existTagIds->unique() as $tagId) {
            $tags[] = [
                'title' => Tag::query()->find($tagId)->title,
                'value' => $tagId
            ];
        }

        return $tags;
    }
}
