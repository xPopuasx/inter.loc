<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * Class Document
 * @package App\Models
 * @property int $id
 * @property string $full_name
 * @property string $description
 * @property string $email
 * @property string $phone
 * @property string $status
 * @property string $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Feedback extends Model
{
    use Filterable, AsSource, Chartable, HasFactory;

    const TYPES = [
        'phone' => 'Обратный звонок',
        'message' => 'Сообщение с формы'
    ];

    const STATUSES = [
        'create' => 'Заявка создана',
        'read' => 'Заявка прочитана',
        'reject' => 'Заявка отклонена',
        'complete' => 'Заявка выполнена',
    ];

    public $fillable =[
        'full_name',
        'description',
        'email',
        'phone',
        'status',
        'type',
    ];

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'file_name',
        'phone',
        'email',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'full_name',
        'description',
        'email',
        'phone',
        'status',
        'type',
        'updated_at'
    ];
}
