<?php

namespace App\Models;

use App\Models\Traits\HasTagsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Pavelrockjob\Filtersortpaginate\Traits\Sortable;

/**
 * @property int $id
 * @property string $coords
 * @property float $square_meters
 * @property float $square_meters_kitchen
 * @property float $square_meters_bathroom
 * @property int $number_of_rooms
 * @property int $has_balcony
 * @property int $has_loggia
 * @property int $has_free_layout
 * @property int $has_separate_bathroom
 * @property int $user_id
 * @property int $house_id
 * @property int $floor_id
 * @property float $price
 * @property float $discount
 * @property int $number
 * @property int $status
 * @property string $image_flat
 * @property string $description
 * @property Floor $floor
 * @property House $house
 * @property string $statusName
 * @property float $live_area_square_meters
 * @property float $ceiling_height
 * @property string $excursion_url
 */
class Flat extends Model
{
    use HasFactory, Filterable, Attachable, HasTagsTrait, Filterable;

    protected $fillable = [
        'row_id',
        'coords',
        'excursion_url',
        'square_meters',
        'square_meters_kitchen',
        'square_meters_bathroom',
        'number_of_rooms',
        'has_balcony',
        'has_loggia',
        'has_free_layout',
        'has_separate_bathroom',
        'image_flat',
        'floor_id',
        'house_id',
        'user_id',
        'description',
        'price',
        'discount',
        'number',
        'live_area_square_meters',
        'ceiling_height',
        'status'
    ];

    protected array $allowedSorts = [
        'status',
        'id'
    ];

    protected array $allowedFilters = [
        'id'
    ];

    public $filters = [
        'square_meters' => [
            'title' => 'Площадь',
            'type' => 'range'
        ],
        'live_area_square_meters' => [
            'title' => 'Жилая площадь',
            'type' => 'range'
        ],
        'number_of_rooms' => [
            'title' => 'Количество комнат',
            'type' => 'checkbox'
        ],
        'has_balcony' => [
            'title' => 'Балкон',
            'type' => 'radio'
        ],
        'has_loggia' => [
            'title' => 'Лоджия',
            'type' => 'radio'
        ],
    ];

    public function getStatusNameAttribute(): string
    {
        if ($this->status == 1) {
            return 'В продаже';
        }

        if ($this->status == 2) {
            return 'Продано';
        }

        if ($this->status == 3) {
            return 'Бронь';
        }

        if ($this->status == 4) {
            return 'Скрыто';
        }

        return '';
    }

    public function floor(): hasOne
    {
        return $this->hasOne(Floor::class, 'id', 'floor_id');
    }

    public function house(): hasOne
    {
        return $this->hasOne(House::class, 'id', 'house_id');
    }

    public function getFilters(): array
    {
        $output = [];

        foreach ($this->filters as $filter => $type) {
            $filterData = [
                'title' => $type['title'],
                'name' => $filter,
                'max' => self::query()->max($filter),
                'min' => self::query()->min($filter),
                'type' => $type['type'],
            ];

            if ($type['type'] == 'checkbox') {
                $filterData['options'] = self::query()->distinct($filter)->orderBy($filter)->pluck($filter)->toArray();
            } elseif ($type['type'] == 'range') {
                $filterData['value_min'] = self::query()->min($filter);
                $filterData['value_max'] = self::query()->max($filter);
            } else {
                $filterData['max'] = self::query()->max($filter);
                $filterData['min'] = self::query()->min($filter);
            }
            $filterData['value'] = null;

            $output[] = $filterData;

        }

        return $output;
    }

    public function getTags(): array
    {
        $tags = [];

        $tagIds = Tag::query()->where('type', Tag::FLATS)->pluck('id')->toArray();

        $existTagIds = collect([]);

        self::query()->with('tags')->whereHas('tags', function ($q) use ($tagIds) {
            $q->whereIn('tag_id', $tagIds);
        })->get()->pluck('tags')->map(function ($tags) {
            return $tags->pluck('id');
        })->each(function ($ids) use (&$existTagIds) {
            $existTagIds = $existTagIds->merge($ids);
        });

        foreach ($existTagIds->unique() as $tagId) {
            $tags[] = [
                'title' => Tag::query()->find($tagId)->title,
                'value' => $tagId
            ];
        }

        return $tags;
    }
}
