<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Platform\Models\User;
use Orchid\Screen\AsSource;

/**
 * Class Document
 * @package App\Models
 * @property int $id
 * @property int $attachment_id
 * @property string $file_name
 * @property int $tag_id
 * @property int $user_id
 * @property int $status_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|User $user
 * @property Collection|Status $status
 * @property Collection|Tag $tag
 * @property Collection|Attachment[] $attachments
 */
class Document extends Model
{
    use Filterable, AsSource, Chartable, HasFactory;

    public $fillable =[
        'file_name',
        'tag_id',
        'user_id',
        'status_id',
        'attachment_id',
    ];

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'file_name',
        'tag_id',
        'status_id',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'file_name',
        'tag_id',
        'status_id',
        'updated_at',
        'created_at',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function status(): HasOne
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function tag(): HasOne
    {
        return $this->hasOne(Tag::class, 'id', 'tag_id');
    }

    public function attachments(): HasMany
    {
        return $this->hasMany(Attachment::class, 'id', 'attachment_id')->where('group','documents');
    }

}
