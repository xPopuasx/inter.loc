<?php

namespace App\Models\Wishlist;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property string $user
 * @property Collection $items
 */
class Wishlist extends Model
{
    use HasFactory;

    protected $fillable = [
        'user',
    ];

    public function items(): HasMany
    {
        return $this->hasMany(WishlistItem::class);
    }
}
