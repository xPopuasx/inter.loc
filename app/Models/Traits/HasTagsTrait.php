<?php

namespace App\Models\Traits;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * @property Tag[] $tags
 */
trait HasTagsTrait
{
    public function tags(string $group = null): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'tagable');
    }
}
