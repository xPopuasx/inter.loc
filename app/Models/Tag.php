<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Platform\Models\User;
use Orchid\Screen\AsSource;

/**
 * Class Type
 * @package App\Models
 * @property int $id
 * @property string $title
 * @property string $type
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection|User $user
 */
class Tag extends Model
{
    use Filterable, AsSource, Chartable, HasFactory;

    CONST POST = 'post';
    CONST DOCUMENTS = 'documents';
    CONST FLATS = 'flats';

    const TYPES = [
        'post' => 'Новости',
        'documents' => 'Документы',
        'flats' => 'Квартиры'
    ];

    public $fillable =[
        'title',
        'type',
        'user_id'
    ];

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'name',
        'type',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'name',
        'type',
        'updated_at',
        'created_at',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @param string $type
     * @return array
     */
    public static function getTagByType(string $type): ?array
    {
        if(!array_key_exists($type, self::TYPES)){
            return null;
        }

        $data =  self::query()->select('id', 'title')
            ->where('type', $type)
            ->get()
            ->toArray();

        if(count($data) == 0){
            return null;
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['title'];
        }

        return $outputData;
    }
}
