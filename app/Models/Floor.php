<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;

/**
 * @property int $id
 * @property int $row_id
 * @property float $coords
 * @property int $number
 * @property int $apartments_in_the_floor
 * @property string $image_floor
 * @property int $house_id
 * @property int $user_id
 * @property string $description
 * @property Collection|null $flats
 * @property House|null $house
 */
class Floor extends Model
{
    use HasFactory, Filterable, Attachable;

    protected $fillable = [
        'row_id',
        'coords',
        'number',
        'apartments_in_the_floor',
        'image_floor',
        'house_id',
        'user_id',
        'description'
    ];

    public function house() : hasOne
    {
        return $this->hasOne(House::class, 'id', 'house_id');
    }

    public function flats() : hasMany
    {
        return $this->hasMany(Flat::class);
    }

    public function user() : hasOne
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
