<?php


namespace App\Services\Telegram;

use Illuminate\Support\Facades\Http;

class TelegramApiService
{
    private string $botUrl;

    public function __construct()
    {
        $this->botUrl = config('bot.api_url').config('bot.token');
    }


    public function sendWebhook(): array
    {
        $response = Http::post($this->botUrl.'/'.'setWebhook?url=https://xn--d1acipgdso.xn--p1ai/webhook');

        return json_decode($response->body(), true);
    }

    public function checkWebhook(): array
    {
        $response = Http::post($this->botUrl.'/getWebhookInfo');

        return json_decode($response->body(), true);
    }

    public function sendBotMessage(array $data = []): void
    {
        foreach (config('bot.chat_ids') as $chatId) {
            $data['chat_id'] = $chatId;

            Http::post($this->botUrl.'/sendMessage', $data);
        }
    }
}
