<?php


namespace App\Services;


use App\Models\Floor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FloorService extends Service
{
    /**
     * Создание или редактирование
     *
     * @param int $userId
     * @param array $request
     * @param Floor|null $model
     *
     * @return Floor
     */
    public function updateOrCreate(int $userId, array $request, ?Model $model): Floor
    {
        $data = $request['floor'];
        $data['user_id'] = $userId;

        return DB::transaction(function() use ($model, $data){
            /** @var Floor $insertData */
            $insertData = (!$model->id)
                ? Floor::query()->create($data)
                : $this->update($model, $data);

            $insertData->attachment()->sync($data['attachment'] ?? []);

            return $insertData;
        });
    }

    /**
     * Удаление записи
     *
     * @param Floor $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        $this->deleteRelate($model);

        return $model->delete();
    }

    /**
     * Редактирование записи
     *
     * @param Floor $model
     * @param array $data
     *
     * @return Floor
     */
    public function update(Model $model, array $data): Floor
    {
        $model->update($data);

        return $model->fresh();
    }

    /**
     * Удаление связей (Квартиры)
     *
     * @param Floor $floor
     *
     */
    private function deleteRelate(Floor $floor): void
    {
        $floor->flats()->delete();
    }

}
