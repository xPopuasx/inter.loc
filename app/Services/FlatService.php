<?php


namespace App\Services;


use App\Models\Flat;
use App\Models\Floor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class FlatService extends Service
{
    /**
     * Создание или редактирование
     *
     * @param int $userId
     * @param array $request
     * @param Floor|null $model
     *
     * @return Flat
     */
    public function updateOrCreate(int $userId, array $request, ?Model $model): Flat
    {
        $data = $request['flat'];
        $data['user_id'] = $userId;

        return DB::transaction(function() use ($model, $data){
            /** @var Flat $insertData */
            $insertData = (!$model->id)
                ? Flat::query()->create($data)
                : $this->update($model, $data);

            $insertData->attachment()->sync($data['attachment'] ?? []);
            $insertData->tags()->sync($data['tags'] ?? []);

            return $insertData;
        });
    }

    /**
     * Удаление записи
     *
     * @param Flat $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * Редактирование записи
     *
     * @param Flat $model
     * @param array $data
     *
     * @return Flat
     */
    public function update(Model $model, array $data): Flat
    {
        $model->update($data);

        return $model->fresh();
    }

    public function list(array $filters): LengthAwarePaginator
    {
        return $this->getWithFilters(Flat::query(), $filters)->paginate($filters['limit'] ?? 5);
    }

    private function getWithFilters(Builder $builder, array $filters): Builder
    {
        if(array_key_exists('tags', $filters)) {
            $builder->whereHas('tags', function (Builder $query) use ($filters) {
                $query->whereIn('tags.id', explode(',', $filters['tags']));
            });
        }

        foreach ((new Flat())->filters as $name => $value) {
            if(array_key_exists($name, $filters)) {
                if($value['type'] == 'range') {
                    $range = explode(':', $filters[$name]);
                    $builder->where($name, '>=', $range[0])
                        ->where($name, '<=', $range[1]);
                }
                if($value['type'] === 'radio') {
                    $builder->where($name, true);
                }
                if($value['type'] === 'checkbox') {
                    $builder->whereIn($name, explode(',', $filters[$name]));
                }
            }

        }

        return $builder;
    }

}
