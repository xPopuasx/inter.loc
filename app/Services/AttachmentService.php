<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;
use Orchid\Attachment\Models\Attachment;

class AttachmentService
{
    /**
     * @param Attachment $attachment
     * @return bool
     */
    public static function deleteStorageFile(Attachment $attachment): bool
    {
        return Storage::disk($attachment->disk)->delete($attachment->path.$attachment->name.'.'.$attachment->extension);
    }
}
