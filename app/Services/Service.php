<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Model;

abstract class Service
{
    abstract protected function delete(Model $model);

    abstract protected function update(Model $model, array $data);
}
