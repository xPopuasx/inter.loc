<?php


namespace App\Services;


use App\Models\Flat;
use App\Models\Floor;
use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class PostService
{
    public function list(array $filters): LengthAwarePaginator
    {
        return $this->getWithFilters(Post::query(), $filters)->paginate($filters['limit'] ?? 10);
    }

    private function getWithFilters(Builder $builder, array $filters): Builder
    {
        if(array_key_exists('tags', $filters)) {
            $builder->whereHas('tags', function (Builder $query) use ($filters) {
                $query->whereIn('tags.id', explode(',', $filters['tags']));
            });
        }

        return $builder;
    }

}
