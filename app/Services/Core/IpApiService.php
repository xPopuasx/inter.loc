<?php


namespace App\Services\Core;

use Illuminate\Support\Facades\Http;

class IpApiService
{


    public function check(string $ip): bool
    {
        $response = Http::get('http://ip-api.com/php/'.$ip);

        $parseResponse = unserialize($response->body());

        if($response->failed() || $parseResponse['status'] !== 'success' || $parseResponse['countryCode'] !== 'RU') {
            return false;
        }

        return true;
    }

}
