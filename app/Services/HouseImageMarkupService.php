<?php


namespace App\Services;


use App\Models\Floor;
use App\Models\House;
use Illuminate\Database\Eloquent\Model;

class HouseImageMarkupService extends Service
{
    /**
     * Создание или редактирование
     *
     * @param int $userId
     * @param array $request
     * @param House|null $model
     *
     * @return Floor
     */
    public function updateOrCreate(int $userId, array $request, ?Model $model): Floor
    {
        $projectData = $request['data'];
        $projectData['user_id'] = $userId;
        $projectData['house_id'] = $model->id;

        $model->status_create_info = 'add_floor_marks';

        $model->save();

        return Floor::query()->updateOrCreate(['house_id' => $model->id, 'row_id' => $projectData['row_id']], $projectData);

    }

    /**
     * Удаление записи
     *
     * @param Floor $model
     *
     * @return bool
     */
    public function delete(Model $model) :bool
    {
        return $model->delete();
    }

    /**
     * Редактирование записи
     *
     * @param Floor $model
     * @param array $data
     *
     * @return Model
     */
    public function update(Model $model, array $data): Model
    {
        $model->update($data);

        return Floor::query()->find($model->id);
    }

}
