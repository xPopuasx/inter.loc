<?php


namespace App\Services;


use App\Models\House;
use App\Models\HouseInformation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HouseService extends Service
{
    /**
     * Создание или редактирование
     *
     * @param int $userId
     * @param array $request
     * @param House|null $model
     *
     * @return House
     */
    public function updateOrCreate(int $userId, array $request, ?Model $model): House
    {
        $projectData = array_merge($request['house'], $request['house']['place']);
        $projectData['status_create_info'] = $this->getStatus(null);
        $projectData['user_id'] = $userId;

        return DB::transaction(function() use ($model, $request, $projectData){
            /** @var House $insertData */
            $insertData = (!$model->id)
                ? House::query()->create($projectData)
                : $this->update($model, $projectData);

            $insertData->attachment()->sync($request['house']['attachment'] ?? []);

            $informationData = array_merge($request['information'], ['house_id' => $insertData->id]);

            HouseInformation::query()->updateOrCreate(['house_id' => $insertData->id], $informationData);

            return $insertData;
        });
    }

    /**
     * Удаление записи
     *
     * @param House $model
     *
     * @return bool
     */
    public function delete(Model $model) :bool
    {
        $this->deleteRelate($model);

        return $model->delete();
    }

    /**
     * Редактирование записи
     *
     * @param House $model
     * @param array $data
     *
     * @return Model
     */
    public function update(Model $model, array $data): Model
    {
        $model->update($data);

        return House::query()->find($model->id);
    }

    /**
     * Удаление связей (доп информаия, вложения)
     *
     * @param House $house
     *
     */
    private function deleteRelate(House $house): void
    {

    }

    private function getStatus(?House $house): string
    {
        if(!$house){
            return 'create_project';
        }

        return 'create_project';
    }

}
