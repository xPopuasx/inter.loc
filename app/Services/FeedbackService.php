<?php


namespace App\Services;


use App\Models\Feedback;
use App\Models\Floor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FeedbackService extends Service
{
    /**
     * @param array $request
     * @return Feedback
     */
    public function create(array $request): Model
    {
        $request['status'] = 'create';

        return Feedback::query()->create($request);
    }

    /**
     * @param Model $model
     * @param array $data
     * @return Feedback
     */
    public function update(Model $model, array $data): Model
    {
        $model->update($data);

        return $model->fresh();
    }

    /**
     * Удаление записи
     *
     * @param Floor $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

}
