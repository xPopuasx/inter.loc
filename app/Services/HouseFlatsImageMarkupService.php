<?php


namespace App\Services;


use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use Illuminate\Database\Eloquent\Model;

class HouseFlatsImageMarkupService extends Service
{
    /**
     * Создание или редактирование
     *
     * @param int $userId
     * @param array $request
     * @param Floor|null $model
     *
     * @return Flat
     */
    public function updateOrCreate(int $userId, array $request, ?Model $model): Flat
    {
        $flatData = $request['data'];
        $flatData['user_id'] = $userId;
        $flatData['floor_id'] = $model->id;
        $flatData['house_id'] = $model->house_id;

        $model->save();

        return Flat::query()->updateOrCreate(['house_id' => $model->house_id, 'floor_id' => $model->id, 'row_id' => $flatData['row_id']], $flatData);

    }

    /**
     * Удаление записи
     *
     * @param Flat $model
     *
     * @return bool
     */
    public function delete(Model $model) :bool
    {
        return $model->delete();
    }

    /**
     * Редактирование записи
     *
     * @param Flat $model
     * @param array $data
     *
     * @return Model
     */
    public function update(Model $model, array $data): Model
    {
        $model->update($data);

        return Floor::query()->find($model->id);
    }

}
