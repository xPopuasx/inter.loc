<?php

namespace App\Console\Commands;

use App\Services\Telegram\TelegramApiService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class Telegram extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create bot wehbhook by name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): void
    {
        /** @var TelegramApiService $service */
        $service = App::make(TelegramApiService::class);

        dump($service->sendWebhook());
        dump($service->checkWebhook());
    }
}
