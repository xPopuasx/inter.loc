<?php

namespace App\Traits;

trait AdministratorRequest
{
    public function messages(): array
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'string' => 'Поле :attribute должно быть строкой',
            'array' => 'Поле :attribute должно быть массивом',
            'date' => 'Поле :attribute должно быть датой',
            'url' => 'Поле :attribute не соответствует формату',
            'date_format' => 'Поле :attribute должно иметь формат d.m.Y',
            'min' => 'Поле :attribute должно иметь минимум :min символов',
            'unique' => 'Такое название уже существует',
            'mimes'  => 'Поле :attribute должно содержать следующий тип файла: :values.',
            'exists' => 'Поле :attribute не найдено',
            'integer' => 'Поле :attribute должно быть числом',
            'numeric' => 'Поле :attribute должно быть числом',
        ];
    }
}
