<?php

use App\Http\Controllers\Api\Feedback\FeedbackController;
use App\Http\Controllers\Api\FlatsController;
use App\Http\Controllers\Api\HouseController;
use App\Http\Controllers\Api\HouseFloorController;
use App\Http\Controllers\Api\HouseFloorFlatController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\WishlistController;
use App\Http\Controllers\Pages\Main\SlideController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'ip'], function () {
    Route::get('flats/filters', [FlatsController::class, 'filters']);
    Route::get('posts/filters', [PostController::class, 'filters']);
    Route::resource('flats', FlatsController::class)->only(['index', 'show']);
    Route::resource('posts', PostController::class)->only(['index']);
    Route::resource('houses', HouseController::class)->only(['index', 'show']);
    Route::resource('houses.floors', HouseFloorController::class)->only(['index', 'show']);
    Route::resource('houses.floors.flats', HouseFloorFlatController::class)->only(['index', 'show']);
    Route::post('feedback', [FeedbackController::class, 'create']);
    Route::get('wishlist', [WishlistController::class, 'create']);
    Route::post('wishlist/items', [WishlistController::class, 'setItem']);
    Route::delete('wishlist/items', [WishlistController::class, 'removeItem']);
});

