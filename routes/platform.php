<?php

declare(strict_types=1);

use App\Models\Document;
use App\Models\Feedback;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\House;
use App\Models\Pages\Main\Slide;
use App\Models\Post;
use App\Models\Tag;
use App\Orchid\Screens\Documents\DocumentsEditScreen;
use App\Orchid\Screens\Documents\DocumentsListScreen;
use App\Orchid\Screens\Feedbacks\FeedbacksEditScreen;
use App\Orchid\Screens\Feedbacks\FeedbacksListScreen;
use App\Orchid\Screens\Houses\Flats\FlatEditScreen;
use App\Orchid\Screens\Houses\Flats\HouseFlatsImageMarkupScreen;
use App\Orchid\Screens\Houses\Flats\HouseFlatsListScreen;
use App\Orchid\Screens\Houses\Floors\FloorEditScreen;
use App\Orchid\Screens\Houses\Floors\HouseFloorsImageMarkupScreen;
use App\Orchid\Screens\Houses\Floors\HouseFloorsListScreen;
use App\Orchid\Screens\Houses\HouseEditScreen;
use App\Orchid\Screens\Houses\HouseListScreen;
use App\Orchid\Screens\Pages\Main\Slides\SlidesEditScreen;
use App\Orchid\Screens\Pages\Main\Slides\SlidesListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Posts\PostsEditScreen;
use App\Orchid\Screens\Posts\PostsListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\Tags\TagsEditScreen;
use App\Orchid\Screens\Tags\TagsListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main')->middleware('activity');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    })->middleware('activity');

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    })->middleware('activity');

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    })->middleware('activity');

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    })->middleware('activity');

// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    })->middleware('activity');

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    })->middleware('activity');

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    })->middleware('activity');

//posts
Route::screen('posts/list', PostsListScreen::class)
    ->name('platform.posts.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список постов');
    })->middleware('activity');

Route::screen('posts/create', PostsEditScreen::class)
    ->name('platform.posts.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Добавить пост', route('platform.posts.create'));
    })->middleware('activity');

Route::screen('posts/{post}/edit', PostsEditScreen::class)
    ->name('platform.posts.edit')
    ->breadcrumbs(function (Trail $trail, Post $post) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать пост',route('platform.posts.edit', $post));
    })->middleware('activity');

//tags
Route::screen('tags/list', TagsListScreen::class)
    ->name('platform.tags.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список постов');
    })->middleware('activity');

Route::screen('tags/create', TagsEditScreen::class)
    ->name('platform.tags.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Добавить тэг', route('platform.tags.create'));
    })->middleware('activity');

Route::screen('tags/{tag}/edit', TagsEditScreen::class)
    ->name('platform.tags.edit')
    ->breadcrumbs(function (Trail $trail, Tag $tag) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать тэг', route('platform.tags.edit', $tag));
    })->middleware('activity');

//documents
Route::screen('documents/list', DocumentsListScreen::class)
    ->name('platform.documents.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список докуметнов');
    })->middleware('activity');

Route::screen('documents/create', DocumentsEditScreen::class)
    ->name('platform.documents.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Добавить документ', route('platform.documents.create'));
    })->middleware('activity');

Route::prefix('pages/main/slides')->group(function () {
    Route::screen('', SlidesListScreen::class)->name('platform.pages.main.slides.list')->middleware('activity');
    Route::screen('pages/main/slides/create', SlidesEditScreen::class)->name('platform.pages.main.slides.create')->middleware('activity');
    Route::screen('pages/main/slides/{slide}/edit', SlidesEditScreen::class)->name('platform.pages.main.slides.edit')->middleware('activity');
});

Route::screen('documents/{document}/edit', DocumentsEditScreen::class)
    ->name('platform.documents.edit')
    ->breadcrumbs(function (Trail $trail, Document $document) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать документ',route('platform.documents.edit', $document));
    })->middleware('activity');

//projects
Route::screen('houses/list', HouseListScreen::class)
    ->name('platform.houses.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список проектов');
    })->middleware('activity');

Route::screen('houses/floors', HouseFloorsListScreen::class)
    ->name('platform.houses.floors.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список этажей');
    })->middleware('activity');

Route::screen('houses/flats', HouseFlatsListScreen::class)
    ->name('platform.houses.flats.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Список квартир');
    })->middleware('activity');

Route::screen('houses/create', HouseEditScreen::class)
    ->name('platform.houses.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Добавить документ', route('platform.houses.create'));
    })->middleware('activity');

Route::screen('houses/{house}/edit', HouseEditScreen::class)
    ->name('platform.houses.edit')
    ->breadcrumbs(function (Trail $trail, House $house) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать документ',route('platform.houses.edit', $house));
    })->middleware('activity');

Route::screen('houses/{house}/floors-image-markup', HouseFloorsImageMarkupScreen::class)
    ->name('platform.houses.floors-image-markup')
    ->breadcrumbs(function (Trail $trail, House $house) {
        return $trail
            ->parent('platform.index')
            ->push('Разметка изображения', route('platform.houses.floors-image-markup', $house));
    })->middleware('activity');

Route::screen('floors/{floor}/edit', FloorEditScreen::class)
    ->name('platform.floor.edit')
    ->breadcrumbs(function (Trail $trail, Floor $floor) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать этаж', route('platform.floor.edit', $floor));
    })->middleware('activity');

Route::screen('flats/{flat}/edit', FlatEditScreen::class)
    ->name('platform.flat.edit')
    ->breadcrumbs(function (Trail $trail, Flat $flat) {
        return $trail
            ->parent('platform.index')
            ->push('Редактировать этаж', route('platform.flat.edit', $flat));
    })->middleware('activity');

Route::screen('houses/{house}/floors/{floor}/flats-image-markup', HouseFlatsImageMarkupScreen::class)
    ->name('platform.house.floor.flats-image-markup')
    ->breadcrumbs(function (Trail $trail, House $house, Floor $floor) {
        return $trail
            ->parent('platform.index')
            ->push('Разметка изображения', route('platform.house.floor.flats-image-markup', ['house' => $house, 'floor' => $floor]));
    })->middleware('activity');

//feedback
Route::screen('feedback/list', FeedbacksListScreen::class)
    ->name('platform.feedbacks.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Обращения');
    })->middleware('activity');

Route::screen('feedback/{feedback}/show', FeedbacksEditScreen::class)
    ->name('platform.feedbacks.show')
    ->breadcrumbs(function (Trail $trail, Feedback $feedback) {
        return $trail
            ->parent('platform.index')
            ->push('Просмотр обращения',route('platform.feedbacks.show', $feedback));
    })->middleware('activity');
