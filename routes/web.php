<?php

use App\Http\Controllers\BotController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index'])->name('web.home.index');

Route::get('/{any}/{id?}/{subAny?}/{subAnyId?}/{product?}/{productId?}', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::post('/webhook', [BotController::class, 'checkMessage'])->middleware('check-telegram-bot-token');
