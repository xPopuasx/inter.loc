<?php

return [
    /*
     * проекты
     */
    'houses' => [
        'types' => [
            'ApartmentHouse',
            'Cottage',
            'ShoppingCenter'
        ],
        /*
         * Типы
         */
        'types_ru' => [
            'ApartmentHouse' => 'Многоквартирный дом',
            //'Cottage' => 'Котэдж',
            //'ShoppingCenter' => 'Торгоывй центр'
        ],
        /*
         * Статусы
         */
        'statuses' => [
            'ApartmentHouse' => [
                'create_project',
                'add_floor_marks',
                'add_room_marks',
                'await_publish',
                'publish',
            ],
            'Cottage' => [
                'success'
            ],
            'ShoppingCenter' => [
                'success'
            ]
        ],
        'statuses_ru' => [
            'create_project' => 'Проект добавлен',
            'add_floor_marks' => 'Добавлены метки подьездов',
            'add_apartments' => 'Добавлены квартиры',
            'await_publish' => 'Ожидает публикации',
            'publish' => 'Проект опубликован',
        ],
    ],
    'geo' => [
        'map' => [
            'n-tagil' => [
                'latitude' => 57.89803377952815,
                'longitude' => 59.96337890625001
            ]
        ]
    ]
];
