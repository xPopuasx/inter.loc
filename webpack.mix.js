const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/project.scss', 'public/css/project.css')
    .sass('resources/sass/admin/editor/editor.scss', 'public/css/admin/editor.css')
    .js('resources/js/admin/editor/editor.js', 'public/js/admin/editor.js')
    .js('resources/js/admin/files/files.js', 'public/js/admin/files.js')
    .js('resources/js/scripts/theme.js', 'public/js/scripts/theme.js')

